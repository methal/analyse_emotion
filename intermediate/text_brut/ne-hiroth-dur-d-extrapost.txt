"
          
Herr Rehrle

          
(sitzt an sim Tisch, wo allerhand fir Zitt’unge
                        lige)

          
Was fir e Handwerk, dü Liawe Zitt’!... was fir e
                        Handwerk!... alle Wuche-n-eso ne Kàsblättle miesse
                        z'schriwe im eso ne Nest, wie unser Städle-n-eins
                        isch!... woher Neüigkeite her nàh? 's kummennt nie nit vor
                        do... ich ha hit vergewes alleweg e Stund g'warte, bis
                        ass e Mürer awe fallt dert àne, wo me sàll Huus baüt,
                        awer se sin anfang-n-Ärger as Seiltànzer... un keine
                        Hàndel, keine Prügel, nit... ich müess in alle-n-andere
                        Zitt’unge ihre-n-Artikel stahle, fir meinen üsz'fille... 's
                        isch g'wiss kei Kilwe, ihr derfe's glaüwe.

          

            
G'sang

            
(Des Morgens in der Frühe)

            
Wenn ich müess mi Zitt’ung schriwe,

            
Bin ich Iwwel dra, 
(bis)

            
Ich thàt's Liawer als lo bliwe,

            
Denn s' isch halteeee wie g'sàit

            
G'wiss kei Fräide.

            
Denn s' isch hait wie g'sàit, 
(bis)

            
Denn, wie g'sàit, 's isch g'wiss kei Fräide.

          

          

            
Wenn ich lieg, so thien d'Lit klaga

            
Un doch derf i halteeee 
(bis)

            
D'Wohret o nitt immer sage,

            
Sunscht isch's o nitt Rechte,

            
's gieng mr schlechtenen.

            
Sunscht isch's o nitt Rechte, 
(bis)

            
's isch nitt Rechte, denn 's gieng mr schlechtenen !

          

          

          

            
Drum heb ich mich an de Wide,

            
Ich nimm d'Scheere-n-als, 
(bis)

            
Fir d'Artikel üse z'schnide

            
Üs de-n-Andre halteeee,

            
Was mir g'fallt.

            
Üs de-n-Andre halteeee, 
(bis)

            
Üs de-n-Andre, was mir g'fallt!

          

          
Un d'Annonce, wo am meiste itrage, die fähle mr o
                            Allewil, die ka-n-i doch nitt üs andere Zitt’unge
                            nah... do ha-n-i jetz wieder e ganze halwe Site
                            läre hit... drum ha-n-i Ordre gen, ass me noch e
                            Stund oder zwei wartet mit em drücen... velicht
                            kummennt noch ebber ebbes kumme lo dri setze... tiens,
                            gradi klopft me Glöüwe a!... wenn's doch e-n-Anonce
                            wàr... Entrez!
(dr Toni kummennt ine)

          

        
"
"
          
Toni.

          
Bin i do bim Herr Rehrle, wo d'Zitung «d'Extrapost»
                        scbribt?... ich mächt ewe-n-ebbes dri setze lo.

        
"
"
          
Herr Rehrle.

          
's isch do, Guten Frinde... setze Sie sich
(er git ein e
                        Stühl. Fir sich)
 à la bonne heure, dà kunnt jetz gradi
                        Rechte.

        
"
"
          
Toni.

          
Nitt wohrer, Herr Rehrle, me ka jo ganz Guten e
                        Hirothsatrag in d'Zitt’ung setze lo?... 's isch jo jetz
                        Mode siter ebes Zitt’...

        
"
"
          
Herr Rehrle.

          
Natirlig, Jungi Mensch, natirlig... me hirothet jo jetz
                        schier nimmig anderst.

        
"
"
          
Toni.

          
Guten... ich bi éwe-n-in dem Fal... schaüe Se, ich müess
                        hirothe, ich ka nimmig warte... ich hätt gern e Fraü un
                        traü doch keine z'froge... wisse Se, ich bi ewe ne
                        wenig schich.

        
"
"
          
Herr Rehrle.

          
Ehen bien, do kanne Se nit bessers mache, als das Ding in
                        «d'Extrapost» z'schriwe... ich will's Ene glich
                        ufsetze... ne Artikel vo-n-ere halwe Site, wie Sie noch
                        keiner g'sàh hàn...

        
"
"
          
Toni.

          
Nei, nei... ich Bitt Se drum... nur e ganz Kläiner... in
                        e-n-Ecke, ung'fàhr eso velicht: Ne Jungi Mensch, wo
                        Huus, Hoft, Schire, Stall, Kieh un Gàns hat, wünscht noch
                        e Fraü derzüe.

        
"
"
          
Herr Rehrle.

          
Was denke Se... das isch nitt genüe... wenn das Ding
                        packe soll, so mien Sie sich e wenig in d'Höche
                        lipfe... lehn Se mieh nur mache... ich will's Ene
                        ufsetze
(er sitzt an dr Tisch un fangt a z'schriwe).

        
"
"
          
Toni.

          
Awer nur nitt z'langer, denn sunscht kostet's gar viel.

        
"
"
          
Herr Rehrle.

          
Nur kei Angst... mr wànn scho iwereins kumme mit-nander.
                       
(fir sich)
 's müess mr mi halwe Site-n-üsfille,
                        kostet's was es will!

        
"
"
          
Toni

          
(fir sich)

          
Wenn's awer nur ebbes nutzt, un ass sich mir glich ebber
                        atràit, derno will ich gern zehl...

          

            
G'sang

            
(Màidele........)

            
's kunnt mr jetz nitt druf a,

            
Wenn ich hirothe ka!

            

            
Wenn ich jetz nur g'schwind 
(bis)

            
Dur Ihre-n-Artikel e Wiwele find,

            
Denn ich bi pressiert, 
(bis)

            
Doch zehl i kei Rappe, wenn's nitt reüssiert.

          

        
"
"
          
Herr Rehrle

          
(springt üf)

          

            
Do gang ich jetz nitt dra,

            
Ich müess mi Gald o ha!

          

        
"
"
            

              
Ich schrib nitt so g'schwind, 
(bis)

              
Wenn ich mi Artikel nitt zahlt genüeg find.

              
Er isch so pressiert 
(bis)

              
Un will doch nitt bleche, wenn's nitt reüssiert!

              
Do gang ich jetz nitt dra,

              
Ich müess mi Gald o ha!

            

          
"
"
            
Toni.

            

              
Wenn ich jetz nur g'schwind 
(bis)

              
Dur Ihre-n-Artikel e Wiwele find,

              
Denn ich bi pressiert, 
(bis)

              
Doch zehl i kei Rappe, wenn's nitt reüssiert!

              
Doch kunnt's mir nitt druf a,

              
Wenn ich hirothe ka!

            

          
"
"
          
Herr Rehrle.

          
Was meinen Se denn eigentlig?... Das Ding müess doch
                        g'schriwe, g'setzt un druckt si... ich ha d'ganz gliche
                        Mieih, eb's packt oder nitt...

        
"
"
          
Toni.

          
Ehen bien, so wisse Se was... packts, so zehl i, un
                        packt's nitt, so zehl i nur d'Hälfte... ich ka doch
                        nitt mi Gald eso-n-eweg werfe, wennn i nit derfir
                        bekomm...

        
"
"
          
Herr Rehrle.

          
Enfin... 's isch mir eithüen... mr wànn Hoffe-n-ass es
                        packt!
(fir sich)
 wenn's nitt wàr wege meinen läre
                        Site...

        
"
"
          
Toni.

          
Also blibt's derbi... wie hàn Sie jetz das Ding
                        g'schriwe?

        
"
"
          
Herr Rehrle

          
(nimmt si Papier)

          
Do isch's... lose Se jetz emol: Ne Jungi Mensch, starick
                        wie ne lab, vo-n-ere Guten Famili, mit eme ag'nehme
                        Charakter, Schön g'wachse
(dr Toni richtet sich stolzi
                        üf un hüeschtet)
 un was nitt z'v'racht-n-isch,
                        Eigethümer vo Huus, Hoft, Stallunge, Kieh, Gàns etc., un
                        wo hofft das alles noch z'vermehre mit dr Hiroth,
                        wünscht mit ere Jungi Person in dr Eh'stand z'tratte, wo
                        o ne bitzi ebbes hat... hein! isch das e-n-Artikele?...

        
"
"
          
Toni

          
(bedenklig).

          
Ich find, 's isch e-n-Artikel! Das kunnt viel z'thir...
                        un 's isch z'langer... 's list's kei Mensch!

        
"
"
          
Herr Rehrle.

          
Mr drücen's mit Rechte grosse Büchstawe, ass es eim Guten
                        in's Aüg fallt...
(fir sich)
 ich will ne scho
                        verwitsche.

        
"
"
          
Toni

          
(angstlig)

          
Nei, nei... nitt mit grosse Büechstawe... 's wird eso
                        scho g'nüeg Platz inàh... wie höcher meinen Se, ass es
                        kunnt ung'fàhr?...

        
"
"
          
Herr Rehrle.

          
's kunnt nitt höcher... ich mach's ganz awe-n-an's End...
                        lehn Se mich nur mache... ich schick Ene derno
                        d'Note... Se werde sàh, ass Se nitt z'viel zehl, fir e
                        Fraü z'biku...

        
"
"
          
Toni.

          
Also zehl i druf hit z'Owe kunnt's dri, nitt wohrer, un
                        nitt züe arg gross ich will's nitt gross...

        
"
"
          
Herr Rehrle.

          
Guten, Guten...
(fir sich)
 isch er stättig, dà Mensch...
                        's hätt mr gradi mi halwe Site-n-üsgfillt!...

          

        
"
"
          
Toni

          
(fir sich)

          
Wenn's nur nitt vergewes isch!... oh! wie freia ich mich,
                        mi Züekünftige z'sàh!
('s Bàwele kunnt ine).

        
"
"
          
Bàwele.

          
Herr Rehrle, ich mächt...
(si sieht dr Toni, fir sich)

                        tiens, ne Jungi Mensch... er isch nitt Iwwel...
(se
                        red' mit em Herr Rehrle).

        
"
"
          
Toni

          
(fir sich)

          
Das wàr jetz eso ebbes!... wer weisst, eb se nitt o im
                        Sinn hat z'hirothe... oh, wenn ich thàt traüe frage...
                        ich will Glöüwe warte dunte, bis ass se furt isch un
                        will derno dr Herr Rehrle froge, was se hat welle...
                       
(läuten)
 also Herr Rehrle, hit z'Owe... Adies bynander...
                       
(er geht üse).

        
"
"
          
Herr Rehrle

          
(rieft em no)

          
Adie!
(züem Bàwele)
 also ne Hirothsatrag soll's si?...
                       
(er ribt si d'Hànd freidig, fir sich)
 gradi fir se gar
                        üsz'fille!

        
"
"
          
Bàwele.

          
Ich ka awer üf Ihre Verschwiegeheit zehl, nitt wohrer?

        
"
"
          
Herr Rehrle.

          
Verschwiege bis in's Grab!... wie miesst me das Ding
                        ung'fàhr ufsetze?...

        
"
"
          
Bàwele.

          
Ich meinen, me kännt's velicht eso schriwe: Ne Jungi
                        Person, üf em Landen üferzoge un wo b'sunders Guten mit em
                        Vieh weisst umz'geh, süecht e Mænner...

        
"
"
          
Herr Rehrle

          
(fir sich)

          
Das isch nitt genüe... 's làngt nitt... ich müess es
                        suche in d'langer z'zieh;
(läuten)
 verzeiht Se, wàr das
                        fir Sie?

        
"
"
          
Bàwele.

          
Worum nitt!... ich bi o nitt vo Holz!

        
"
"
          
Herr Rehrle

          
(springt üf, fir sich)

          
E-n-Idee... dr Ander ka no nitt wit si... wenn ich em
                        Die thàt ahànke, fir secher z'si, ass ich zahlt
                        wird?...
(er lüegt züem Fenster üse)
 richtig, dert isch
                        er noch... bst! Guten Frinde... warte Se noch e wenig
                        im Vorhüs, i ha noch ebbes mit Ene z'rede...

        
"
"
          
Bàwele

          
(fir sich)

          
Was isch em... was hat er?

        
"
"
          
Herr Rehrle

          
(sitzt wieder)

          
Also, hà mr g'sàit, e Jungfre, wo mit em Rindvieh
                        weisst umz'geh, süecht e Mænner...

        
"
"
          
Bàwele.

          
C'est çà... das geht alles üf ei Linie, nitt wohrer? un do
                        wird's jo nitt so viel koste?

        
"
"
          
Herr Rehrle.

          
Was denkt Se... üf ei Linie! nitt emol üf zwei... un
                        derno mie mr o das Ding e wenig anderst dràihe... 's
                        isch e wenig...

        
"
"
          
Bàwele

          
(underbrecht ne)

          
Z'kurz, nitt wohrer?... Sie hätte gern e langer Litenei, wo
                        Ihne 's Journal üsfillt... 's isch ganz Guten eso...
                        setze Sie's nur eso dri.

        
"
"
          
Herr Rehrle

          
(fir sich)

          
Dà wo Die bikunnt, brücht o nitt sage «Gott Stroof mi!»
                       
(läuten)
 Guten, Guten... wenn Sie sich eso will drücen lo,
                        mir isch's scho Rechte.

          

        
"
"
          
Bàwele

          
(fir sich)

          
Hein, was sàit er, ich will mich drücen lo?... wie meint
                        er das?

        
"
"
          
Herr Rehrle.

          
Mr sage-n-also :
(er schribt)
 Ne Jungi Person, im Stall
                        uferzoge...

        
"
"
          
Bàwele.

          
Was, im Stall!... üf em Landen, ha-n-i g'sàit!

        
"
"
          
Herr Rehrle.

          
's kunnt üf's Gliche-n-üse... also üf em Landen üferzoge
                        un wo « d'Viehzucht versteht, » wà mr do mache, 's isch
                        e wenig civilisierter, süecht sich z'verhirothe mit eme
                        Mænner.

        
"
"
          
Bàwele.

          
Natirlig mit eme Mænner... das isch alles Üsfillete!...
                        mache Sie nur eenfach « süecht e Mænner » un dermit ferig.

          

            
G'sang

            
('s kummennt e Vogel)

            
Wenn e Mænner kummennt kumme z'fliege

            
Un kneit hi vor mi Füess,

            
So will ich ne glich ziege,

            
Ass er mir folgen müess.

          

        
"
"
            
Bàwele.

            

              
Liawe Mænner, will ich sage,

              
Wenn D' e Fraü witt in's Huus,

              
So derfsch d'Hose nitt trage,

              
Zieg se Liawe glich üs.

            

          
"
"
            
Herr Rehrle

            
(fir sich).

            

              
Wohl sin's Artikel zwei,

              
Nur isch dr letschte z'klei,

              
Doch fir was ich vor ha,

              
Isch noch g'nüe dra.

            

          
"
"
          
Herr Rehrle.

          
Sa, do wàr's denn jetz eso g'schriwe... wenn Sie's
                            velicht noch will iwerlese 
(er git ere im Toni si Artikel)
ich will siter mit dem Jungi Mensch ferig mache 
(er geht geje dr Thüre).

        
"
"
          
Bàwele

          
(nimmt 's Papier, fir sich)

          
Ich bi jetz doch wunderfizi, eb ich üf dà Artikel
                            hi ne Mænner bekomm...

        
"
"
          
Herr Rehrle

          
(macht d'Thüre-n-üf)

          
Jungi Mensch, kämme Se noch e wenig un iwerlese
                            Se-n-Ihre-n-Artikel, fir secher z'si, ass nit fahlt
(dr Toni kunnt ine, er git em im Bàwele si
                            Artikel)

        
"
"
          
Bàwele

          
(fangt a z'lese)

          
Ne Jungi Mensch... hein, was git er mir do
                            z'lese...

        
"
"
          
Toni

          
(fangt a z'lese)

          
Ne Jungi Person...

        
"
"
          
Bàwele

          
(fahrt furt)

          
Stark wie ne lab...

        
"
"
          
Toni.

          
Uf em Landen uferzoge...

        
"
"
          
Bàwele.

          
Un wo mit dr Hiroth...

        
"
"
          
Toni.

          
D'Viehzucht versteht...

        
"
"
          
Bàwele.

          
Wott in dr Eh'stand tratte...

        
"
"
          
Toni.

          
Süecht e Mænner...
(se lüege enander a)
tiens, tiens!...

        
"
"
          
Bàwele.

          
Tiens, tiens!...
(fir sich)
 me kännt's jo prawiere...

        
"
"
          
Toni

          
(geht verlege geje-n-em Bàwele)

          
Hm, hm... wenn Sie velicht wott...

          

        
"
"
          
Herr Rehrle.

          
Mache doch nitt soviel Stàmpenei-n-un Manöver, gàn
                            enander d'Hand un e Verschmutze un dermit ferig...
                        

        
"
"
          
Toni

          
(fallt im Bàwele um dr Hals)

          
Liawe Brüt!

        
"
"
          
Bàwele.

          
Liawe Hochziter!

        
"
"
          
Toni.

          
Herr Rehrle, Sie brüche jetz mi Annonce nitt
                            z'drücen... 's isch unnöthig... ich ha jetz e Fraü.

        
"
"
          
Bàwele.

          
Un meinen-n-o nitt... ich ha jetz e Mænner.

        
"
"
          
Herr Rehrle.

          
O sapristi... was ha-n-ich ag'stellt!... an das
                            ha-n-ich jo gar nitt denkt... un mi halwe Site, wo
                            läre isch... lose, gen mr üf's wenigst nur
                            d'Autorisation, eüer Hirothsg'schichte
                            z'verzähle... ich bi versichert, ass derno alle
                            Jungi Lit üs dr ganze Umgegend kämme kumme Annonce
                            mache-n-in « d'Extrapost »...

        
"
"
          
(mit-nander)

          

            
's isch g'wiss 'e Guten Mode,

            
Das müess me g'steh,

            
Dur « d'Extrapost » z'hirothe,

            
Denn schnalle thüet's geh.

            
Will me küm sich drücen lo,

            
Steht o scho ne Fraü/Mænner als so,

            
's geht ganz Guten eso. 
(bis)

          

        
"
"
          
(mit-nander)

          

            
's isch g'wiss 'e Guten Mode,

            
Das müess me g'steh,

            
Dur « d'Extrapost » z'hirothe,

            
Denn schnalle thüet's geh.

            
Will me küm sich drücen lo,

            
Steht o scho ne Fraü/Mænner als so,

            
's geht ganz Guten eso. 
(bis)

          

        
"
"
          
(mit-nander)

          

            
's isch g'wiss 'e Guten Mode,

            
Das müess me g'steh,

            
Dur « d'Extrapost » z'hirothe,

            
Denn schnalle thüet's geh.

            
Will me küm sich drücen lo,

            
Steht o scho ne Fraü/Mænner als so,

            
's geht ganz Guten eso. 
(bis)

          

        
"
