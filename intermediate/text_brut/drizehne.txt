"
          
Edi

          
(kummennt-hinte-n-ine-n-im Frack un wisse Hàndschig, er
                        hat e Liecht in dr Hand un stellts üf dr Tisch)

          
Endlig sin jetz uns're Huchzyttsgäst emol heim geh
                        schlofe un ich bin allei, ass ich doch ka meinennnn-n-Idee
                        ne wenig z'àmmesüeche!... g'hirothe siter dà Morjen-n-am
                        Elfe!... isch's denn wirkligii wohrerer un isch mi Winsch, mi
                        häisser Winsch, denn wirkligii in Erfillung gange!...
                        nei, 's isch kei Traürn, denn do isch jo dr Ehen'ring...
                        siter am Elfe dà Morjen isch 's Lenele mi Fraü...
                        d'Huchzytt hà mr dunte g'halteeee, im Erste, bim Lenele
                        sin're Tante... 's isch jetz dert, 's Lenele, in dr
                        Schlofstuwe
(er zeigt links)
 's zieht si Kranz ab!...
                        ich ha-n-em awer Verspräche, ass mr ne-n-üfbewahre
                        unde eme Globe, ass kei Staüb dra kummennt... tiens... do
                        ligt ein vo sine Hàndschig... 's isch schint's noch do
                        g'si im salon, vor ebb's ins Schlofzimmer isch... 's
                        isch ewe vor mir üfe, un vor ebb's gange-n-isch,
                        ha-n-ich ihm miesse Versprechen, ass ich nitt vor de
                        Zwölfe-n-in d'Schlofstuwe kummemme...
(er lüegt üf si ühre)

                        's isch jetz halwer!... o mi Lenele!... ihr
                        känne-n-eüch kei begryffe mache, wie gern ass ich's
                        ha!... das Ding hat scho langerer dürt im stilli... ich
                        hätt's ihm scho langerer gern g'sàit... doch endlig am
                        letschte Ball hat's üse miesse... ich hätt nimmig
                        langererer känne schwige...
                    

          

          

            
G'sang

            
Liawe Kind, ha-n-ich ihm g'sàit,

            
Dü bisch allei mei Fräide,

            
Ich ka jetz nimmig si,

            
Wenn ich nitt bi Dir bi;

            
Wenn Dü mich gern witt ha,

            
So lüeg mich zàrtlig a

            
Un zeig mir in Dim blick

            
Mi allergrösstes Glüeck.

          

          

            
Ohne Dich

            
Wàr fir mich

            
Alles läre,

            
Um mich her,

            
Doch mit Dir

            
Làchelt mir

            
Alles do

            
Gar so froh!

            
}bis

          

          
Un jetz isch's mi Fraü!... mi Fraü!... wie mich das
                            possig dunkt, wenn ich sag « mi Fraü »... ich müess
                            mich jetz halteee doch dra g'wöhne... 's isch doch
                            ebbes Schön's um d'Liawe... un doch isch's
                            Sonderbare!... z'erst ha-n-i 's Bàwele gern g'ha...
                            derno 's Gretele... üf eimol awer ha-n-i 's Lenele
                            gern bikumme... awer jetz spir i, ass es fir Allewil
                            isch... doch b'sinn ich mich jetz, ass ich 's
                            gliche g'spirt ha bim Bàwele un bim Gretele...
                            enfin, was will me halteee... 's isch in dr Natür...
                            's Lenele isch 's' Schönste Kind, wo me will sàh...
                            wà mir glückliche si mit-nander do owe in unserm
                            Kläiner Paredies!... denn gross isch 's nitt, 's
                            isch wohrerer... e-n-Esszimmer, e Schlofzimmer un e
                            salon... was brüche mr mehr?... Ich hätt nie
                            Glöüwet, ass 's Paredies gemachti isch mit
                            eme-n-Esszimmer, e Schlofzimmer un e salon... mr
                            brücht schint's kei Garte, un we mir Aepfel esse
                            wànn fir 's Dessert, so hat uns dr Hüsmeister erst
                            nitt emol nit z'sage in unsrem do... 
 sunscht kinde
                        mir em üf...
(er lüegt üf si Ühre)
 erst fünf Minüte ab halwer... wie die Zitt’ so langerersam umme geht... o was fir e Tag... ich ha g'meint, er will kei End meh nàh!...

          

            
G'sang.
            
            
Vor ebb dr Tag isch kummemmee,

            
Laüf ich jetz hit scho umme,

            
Ich ha schier d'ganze Nacht

            
Vor Fräide kei Aüg züegemachti;

            
Ich ha mir d'hoor lo stutze

            
Un gli dr Kopf lo buetze

            
Un ha mich bol derno

            
Züer Brüt heimfiehre lo,

            
Was fir e Tag un was fir Cumplemente!

            
Ich ha schier Glöüwet, er will gar nimmig ende;

            
Bol dert, bol do

            
Sin d'Cüsin un d'Verwandte

            
Uns zwei als no,

            
Hàn uns nitt rüehig g'lo!

            
Do hat nit g'nutzt,

            
Se hàn d'Brüt g'schmutzt,

            
Ich hätt bol trutzt

            
Mit de Frinde un bekannte,

            
Do hat nit g'nutzt,

            
Se hàn d'Brüt g'schmutzt,

            
Ich hätt bol trutzt;

            
Ich allei ha nitt g'schmutzt!

          

          
Dr grosse Lipps b'sunders, dr Cüsin vo minre Fraü...
                            hein, wie-n-ich's jetz scho g'wohnt bi, fir z'sage
                            « mi Fraü »!... Dr grosse Lipps, denn b'sunders
                            isch im Lenele nog'schliche dr ganze Tag... er
                            hätt's ewe-n-o gern, g'hirothe... awer « schüit »
(er fahrt mit em Finger unde, dr Nase durch)
ich ha-n-em gli g'sàit, « De brüchsch Di nitt z'melde mit Dim Kopf wie ne dicke Riewe... De fallsch derdur, Lipps »... d'abord ha-n-ich scho ne 
Schönere Position...
(stolzi)
 Chef de Bureau!... un derno, ohne mich z'flattiere, bin ich Glöüwe o ne wenig besser...
(er b'schaüt sich im Spiegel)
 décidément, jà, ich bi viel besser... dr Lipps isch e Tapp-ins-Mües... hat er nitt hit, wo me-n-em hat welle am Esse dr erste Wi ischenke, sie klei Glasle präsentiert... er hat g'sàit, er will 's grosse b'halteeee fir dr Guten!... er ka-n-en ewe vertrage... hinte-n-un vorne-n-isch er hit Allewil um 's Lenele-n-ume gsi!... un am Dessert hat er natirlig o welle de Strumpfbàndel hole... dert ha-n- ich ne awer derno verwischt... denn wo-n-er unde em Tisch grübt isch, so ha-n-ich üf emol meinennn Bei üsg'streckt un bin em derno, ass wie par hasard, an si grosse Nase g'stolpert...

          

            
G'sang.

            
Ich ha's ganz Guten g'merkt am Tisch,

            
Ass er will dr Bàndel hole,

            
Wo-n-er drunde krobelt isch

            
Wie ne Schelm, eso verstohlene.

            
Ich ha ihn derno

            
Noch viel mehr Glöüwe g'hasst,

            
Ha-n-em awer o

            
Wege dem üfpasst,

            
Hat scho ebber g'sàh

            
Denn scho so ne Klette?

            
's müess d'Geduld dem nàh,

            
Wo am liebste spasst!

            
Wart nur, ha-n-ich denkt,

            
Das isch Dir nit g'schenkt,

            
Un ha-n-ihm eso

                        
Ein's üf d'Nase g'làngt!

          
Ich ha mich halteee derno natirlig glich excüsiert...
                            du reste, 's sin nur Vernischüeh gsi... g'wiss...
                            me macht sich kei Idee, was eso ne-n-arm
                            Huchzytter müess dure mache!... dà Morjen scho, wo
                            mir üs dr 
Kirche kumme sin, hàn se durchüs alle welle
                            spazierefahre üf Habse... ich wàr Liawe d'heim
                            bliwe... oder mit em Lenele-n-allei üf Habse
                            g'fahre... wenn se mi derno noch hätte lo züe-n-em
                            sitze in dr Kütsche!... awer nei... es isch in eim
                            Ecke gsi un ich im and're üf dr and're Site!... se
                            hätte mi Glöüwe üf dr Bock mache z'sitze züem
                            Kütschner, wenn se hätte känne!... und derno... hàn
                            se mir nitt noch hit z'Owe d'Bettlade üsnander
                            g'strübt, die Halunke!... awer eso fin, as wie sie,
                            bin ich o noch... vorig, ebb mi Fraü üfe-n-isch,
                            bin ich g'schwind noch emol geh lüege dowe, eb
                            alles in Ordnung isch... un bi derno üf das Ding
                            g'rothe un ha se wieder inander g'strübt... üf die
                            Art, wenn derno die Herr meinennn... hà hà hà... so
                            meinennn se ganz làtz... wer z'letscht laachter, laachter am
                            beste!...
(er lüegt üf si Ühre)
noch zwanzig Minüte!... se wird doch ebbe nitt steh?... 
(er lost)
 nei... 's macht tic tac, tic tac... as wie mi Puls... er geht famos schnalle, mi Puls... ich Glöüwe, ich ha 's Fiewer.

          

            
G'sang.
            
            
Wenn dr Puls eso schnalle schlat,

            
Heisst's, me hat's im höchstenn Grad,

            
Wo kummennt das bi mir jetz her?

            
Isch's nitt ung'fàhr, 
(bis)

            
Wil mr 's Lenele so zart

            
blickt hat üf e g'wisse-n-Art,

            
Wo bim Tänzer, sät an ihm a,

            
Ich 's Hàndle-n-ihm druckt ha?

            
Froh un glückliche si mir galoppiert drüf los,

            
Hand in Hand, 's isch jetz e Viertelstund nur bloose.

            
Wie im Sturmwind hat is d'Musiken mit si gnu,

            
Un so ka me 's Fiewer scho bikumme;

            
Wenn dr Puls eso schnalle schlat,

            
Heisst's, me hat's im höchstenn Grad,

            

            
Wo kummennt das bi mir jetz her?

            
Isch's nitt ung'fàhr, 
(bis)

            
Wil als bis am Zwölfe z'Nacht

            
's Fiewer g'wöhnlig Progrès macht?

            
Das isch o bi mir dr Fal,

            
Ich spir's jetz iwerall!

          

          
Mr wànn awer Hoffe, ass es wie alle Fiewer geje
                            Morjen wieder abnimmt...
('s schlat zwölfe-n-üf dr Wandühre, dr Herr
                                Liaweling zählt un schaüt derno üf sine)
Ehen... ich ha jo erst dreiviertel!... un doch ha-n-i alle beide mit-nander g'richtet dà Morjen... 
(er laachter)
 aha, ich merk's... 's Lenele-n-isch do gsi un hat se ne Viertelstund fire g'richtet... Hàndschig hat's verrothe!... 
(er läscht 's Liecht)
 Guten Nacht... schlofe wohler 
(er springt ins Schlofzimmer).

        
"
