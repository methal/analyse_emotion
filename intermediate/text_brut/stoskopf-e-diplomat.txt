"
        
 Marx: 

        
(allei, an der Mitteltüre horchend. Mænner hört zankende Stimmen)

        
Bon, diß isch wid’r e Leitskrambol, was mueß jetz do schun wid’r los sin?! Natierli — ich hab m’r’s doch glich gedenkt. — S’isch mini Fräu, wie mit ihrer Schweschter, mit'm Tant’l, Händel hett. — 
(Seufzt)
 Wenn äner än Fräu hett, ze reichts als for gewöhnli gradi, wenn äner awer zwei um sich erum mueß han wie ich, brücht er nit ze säuje „Gott Stroof mi“, er isch g’Strooft genue ! — 
(In befserer Lünne)
 Jetzt, ’s Tantel diß erwe m’r emol oder au moins unsri King, un do kann m’r sich halte so e manches g’falle Ion. — ’s hett iwrigens Hitzig koscht bis m’r so witt gewahn sin, ich hab alli Ränk und schlich an muehn wende, um se vum Hierothe ab wendisch ze mache, Meh wi zeh Mol hawi se reujes gemachti, un wie, es isch als nit so leichte gewahn. — Jetz isch d’Partie Gott sej Danke Gewinne, sie isch iwwer'm alter drüsse, sie isch e-n-alti Schachtel, un vom Hierothe kann kenn Redd meh sin.

      
"
"
        
Frau Marx: 

        
(kommt hastig in die Stube gestürmt)

        
Siehsch, ich hab noch Allewil gsäit, sie macht noch emol e Dummheiten, s’ isch wäjer d’rwert, daß m’r sich so veel hett g’falle Ion um se !

      
"
"
        
Marx:

        
Ja‚, was Deifels isch denn do schun wid’r los?!

      
"
"
        
Frau Marx:

        
Was los isch?! — Diß Mensch isch verrückten, säuj i d'r, verrückten! — Hierothe wills! Hierothe !

      
"
"
        
Marx:

        
’s Tantel hierothe? — ’s Babette hierothe!

      
"
"
        
Frau Marx:

        
Ja‚, hierothe !

      
"
"
        
Marx:

        
Ich Glöüwe, dü bisch maschokke, do möcht i awer äu d'rbie sin !

      
"
"
        
Frau Marx:

        
Ja‚, ewe gradi im Läuf vun-ere Dischküssion hett se m’r's üf de Flade g’striche, un wie, Morjen hett se schun Verschriewung. Alles hett se diß Dür hinger ünserm Rücke gemachti, alles. Diß mol, hett se gsäit, macht se neemes meh röujies.

      
"
"
        
Marx:

        
Numme ruehig Bluet. — ’s Tantel hierothe?l Nee! — 
(schüttelt den Kopf und laachter)
 — loß mich numme mache, ’s wurd kenn Bruehj so heissa gesse wie se kocht wurd! —

      
"
"
        
Frau Marx:

        
Do isch neemes shuldi dran wie d’Ammej, die alt Stündlere, wie zue minere Schweschter Allewil sät, daß se-n-in kem chrischtliche Huus Iebt. Do hesch’s jetzt! — Marx 
(schüttelt wiederholt den Kopf und laachter)
! So g'schwind scheese d’Preiße nit.

      
"
"
        
Frau Marx:

        
Ja‚, un weisch äu, wer’s isch ?

      
"
"
        
Marx:

        
Ja‚? — denne möcht i kenne, wie so e Gschmack hett 
(laachter)
,

      
"
"
        
Frau Marx:

        
’s Lutze Schlosser, d'r Stündler! Marx 
(laachter läuten auf)
 : Was, der alt Bluttkopf denkt noch an’s Hierothe — Na, die täte jetzt zamme passe wie e Füscht uffe-n- Au 
(laachter)
. Diß gäb e Päärl! Denne Charivari möcht ich sehn.

      
"
"
        
Frau Marx :

        
(weinerlicher)

        
Brüchsch äu noch ze Lache, ’s isch ari genue ! Dü wäisch, daß wenn emol e-n-alti Schier Feuer fangt, se nimmi ze läsche- n-isch.

      
"
"
        
Marx:

        
Loß mi numme mache, do wurd nix drüs: — Tiens, 
(am Fenster schauend)
 do kommt gradi ’s Tantel, jetzt nit gemückst, Ioß mi mache un speel dini Roll Guten, ich mach jetzt wie wenn i mit d'r Händel hätt! — 
(Babette herein; Babette ist eine Aelterer Jungfre und steht komischer aus. Marx zu seiner Frau in heftigem Tone, wie wenn er im größten zornig wäre)
 Ich hab d’r’s schun hundert Mol g'säit, diß Läwe hawi sät mit ejerm ewige rum un num händel, ich will min Läwe in Fredde b’schließe un nit alle Daö de Derikekrej im Huus han! — Dü muesch äu Allewil dini Schweschter chikaneere !

      
"
"
        
Frau Marx:

        
Nit emol von sim Mænner wurd m’r meh souteneert, dü hesch noch Allewil meinene Schweschter Rechte gen! 
(geht ab und tut dergleichen als würde sie Weinen)
. Marx 
(zu Babette gewandt)
 : G’schwej, diß Ding kann un derf nimm so furt gehn! Früehjer wie i Jungi gewahn bin, hett’s m’r wenjer gemachti, wenn er de ganze Daö mitander erumdeschpeteert han, awer jetzt, wie i alter wurr, möcht i mini Ruehj han, un do gitts numme-n-än Mitt'le, Babette, ihr muehn hierothe ! un for’s i frank erüs ze säuje, ich hab e famosi Partie in Uessicht, e Jungi Wittmann ohne King. Ere hett ejch g’sahn, un ihr täte sini Affär mache. Babette 
(affektiert: ’s isch ze spoot, ich bin Verlobten, un im Äujieblick wurd meinen Huchzytt kumme! Marx 
(tut sehr verwunden)
? Isch’s mögli? Was säujee-n-ihr do ? Tant mieux, mes félicitations schüttelt ihr Kraft die Hand)
, Am so besser, wenn’r selwer äine g’funge han. ’s isch Allewil besser m’r hieroth üs Leb. Ich bin noch stets geje d’ verkuppelte Huchzytt gewahn, wo’s Vermeje de Häuptüsschlag gen hett. — Kenn ich ejre Huchzytt wenigstens ? ? Babette 
(verlegener)
 : Ja‚ — rothe-n-emol!

      
"
"
        
Marx:

        
Ja‚, was kann i do rothe, ich seh, daß'r Rechte verleebt sin. Diß isch Alles. Ich denk, wenn’r äner erüs g’suecht han, ze wurd’s e nett suufer Burscht sin, wie zue ejch paßt 
(Babette ist sehr erfreut und geberdet sich wie ein schüchternes junges Mädchen)
, Ich hab halwer Vrdacht üf . . . 
(macht eine längere Paus', als wollte er die Antwort auf ihrem G'sicht ablesen)
 ’s Chrischtian Marte.

      
"
"
        
Babette :

        
(enttäuscht)

        
Jo, wie denke-n-ihr anne? — der isch veel ze Jungi, ich Glöüwe ihr sin nit Rechte g'schied! —

      
"
"
        
Marx:

        
Was, säuje-n-ihr do, ze Jungi?! — Ja‚, sin ihr denn ebbe-n-alt?! Mit ejerm Vermeje un ejerm alter könnte-n-ihr noch e Jungier han, und for d' Wohret ze säuje, zett’r daß ich ejch kenn, sin’r wie m’r schient, un wenn er ejch nit e so altmodisch mutze täte un e bissl meh ,toilette'' mache un ejch à la chien strähle täte wie's Üschenie, ze kennt m’r ejch for e Person so in de Üsgangs zwanzig nemme. Babette 
(geschmeichelt)
 : Il y a du vrai ! —

      
"
"
        
Marx:

        
Anderi suche sich Jungi ze mache, un Ihr mache-n-ejch mit gewaltige alter! — Mit dem allem weiß i jetzt als noch nit, wie ejer Huchzytt heitzt! 
(Babette zögert)
 alle-n-erüs mit d’r Sprooch! —

      
"
"
        
Babette:

        
Et bien, daß 'r’s wisse, 's isch... 
(halte inne)
 jo, ich tröuj’s nit ze säuje — ’s isch — ’s isch s’Lutzze Schlosser! Marx 
(tut sehr verblüfft; schaut sie verwundert an und schlägt die Hände über dem Kopf zamme)
; Wa . . . Was? — s’Lutze Schlosser, hör ich Rechte?! — Der hett jo e Bluttkopf! Was, Ihr nemme-n-äne mit'm e Bluttkopf ? Wenn’r noch wenigstens e Barreck hätt, daß’r nit zü alt for ejch üs thät sahn, un zue ejch passe thät! — D’rzue Schlosser, so e ruessigs Handwerik ! — Un no, fascht kenn Vermeje un Allewil angethon, daß m’r'm e Sü möcht gen üf dr Stoß. Babette ! Babette ! Ihr mit ejerm schen Vermeje !! — Ihr mit ejerer Edükation — do hätt mini Partie besser for i ge- paßt — mais, d’Leb hett emol sini appartigi Nübbe, un wenn m’r verleebt isch, isch m'r blind — Excusez- moi, ich hab meh g’säit, daß i hätt säuje selle ! —

      
"
"
        
Babette:

        
Ho, von Leb isch nit gradi d’Redd ! Un for d' Wohret ze säuje, ich hab même langer nit ja welle säuje, awer d’Ammej üs’m Stünd’l hett m'r kenn Ruehj gelon.

      
"
"
        
Marx :

        
(für sich)

        
Die Canaille! 
(zu Babette)
 Ich ver- steh d’ Ammej ganz Guten, wenn m’r's Rechte bedrachte, ze mache-n-r e Gutens Werik, wenn’r de Schlosser hin- rothe. Denn ’s isch e Misère gewahn ze sehn, wie'r verwahrlost erumgange-n-isch. — Uff alli Fal derft'r m’r an ejrem Platz nemmi in sim ruessige Blüs un mit'm Glowe-n-im Mül erumläufe, wenn’r emol g'hinroth sin. Wenn äner e chance hett, so e Guteni Partie ze mache wie der, ze derf’r sich schun ebs g'fälle Ion, denn mit sim Vermeje soll’s nit wytt here sin, so viel i weiß.

      
"
"
        
Babette:

        
Diß hawi äu shun gedenkt. Kenn Angst, bie d'r nächste Geläjeheit wur i's’m säuje, wie’r sich ze verhalte hett, 
(Mænner hört Schritte von draußen)
 Babette an’s Fenster. Herrjeh, do kummet’r schun, der isch me- neidi pressiert. 
(Zu Marx)
 Säuje-n’m, er sell warte, ich kumme gradi, ich will noch e bissl ,toilette'' mache; 
(für sich)
 ’s isch wohrer, min Schwöujer hett Rechte, ich bin noch nit so alt, un wenn i mi e bissl erüsstaffeer, ze seh i noch ganz Jungi üs, ich mueß im Schlosser e bissl ze verstehn gen, daß i noch jüngeri kennt han as er, un daß ich mich nit um ne riß 
(links ab)
, Marx 
(allei)
; Maintenant soyons malin! 
(es klopft)
 Entrez ! 
(Schlosser Lutz tritt herein in altmodischem Cylinder und Gehrock)
.

      
"
"
        
Lutz:

        
Nix for unguet, daß i kumme derangeere.

      
"
"
        
Marx:

        
Guten Daö, kinftiger Schwöujer ! 
(streckt ihm)

      
"
"
        
Lutz :

        
(verwundert)

        
So, ich hab eich e Nejigkeit welle bringe, un wie i seh, kenne-n-er se schun!

      
"
"
        
Marx :

        
(laachter)

        
 Diß hawi schun Iang gewißt, daß’s  e so kummt pfiffig verlicht vor ejch!

      
"
"
        
Lutz :

        
(sehr verwundert)

        
Oho!

      
"
"
        
Marx :

        
(pfiffig)

        
Schriewe-n-ejch diß hinger d’Ohre, e Metzjeri weiß e jedi Küh, wie ze verkäufe-n-isch, un e jedi Partie, wie ing’fädelt wurd, 
(mysteriös)
 un b’sonders, wenn m’r selwer infädle hett hälfe! —

      
"
"
        
Lutz :

        
(verwundert)

        
Was, Ihr! Ich versteh nit! —

      
"
"
        
Marx:

        
Ei jo, wie d’Sache jetzt so witt sin, ze kann i's jo g’stehn! —

      
"
"
        
Lutz:

        
Do kumme i jetzt nit drüs erüs — ejri G'schwej hett m’r doch Allewil g’säit, ihr derfe nix d'rvun wiffe, denn ihr thäte wid’r e Stecke-n-in’s Rad stecke! Marx 
(ganz vertraulich, nimmt den Schlosser unde den Arm)
; Allons voyons, löuje mich an ohne ze Lache; ich e Stecke-n-in’s Rad stecke, wenn ich schon zetter zwei Johr min Möjlichst mach for se ze verschieße?! — Mäne denn ihr, daß 's e G’spaß isch, mit zwei Wiebslitt ze Iewe, wie sich de ganze Daö in de Hoore lejie! Ewe han m’r gradii wid'r de Derikekrej im Huus g'hett! — So e-n-alti Jungfre isch haltee gar ze wun- derli, un mini Schulde isch’s weiß Gott nit, daß se noch nit g’hieroth isch, ich suche schon langer, wie g'sait, s’hett awer nimmi Rechte welle packe, s’hett kenner meh an welle bissi. s’ Tantel isch haltee jetzt schun starick üf de Johre, un ’s isch bekannte, daß se s’Rejement im Huus füehrt! Jetzt, diß weiß ich, do löuje-n-ihr nimm so drüf, drum beni froh, daß ihr ejch üf denne Punkt verständigt han. Ihr passe gradii zamme, ’s hätt sich nit besser treffe könne, ihr han e ruehige, Guten Charakter, sin noochgäwig un sin ge- wohnt, e bissil undem Pantoffel ze stehn vun ejere verstorwene Scweschter here. D’rno höre-n-er äu e bissl Iwwel zue Zitt’, so veel ich weiß. Diß isch äu vun Vorthäl, wenn se-n-ejch emol de Kawes eralest. Ich hab schun manchmol gewünscht, ich wär täub wie e Trompeterschimmel!

      
"
"
        
Lutz:

        
Oho, so täub bin i mit! — Ich hoer numme-n-Iwwel, wenn’s andersch Wetter gitt.

      
"
"
        
Marx:

        
Ich mach numme G’spaß, wie g’säit, ihr passe Guten zamme, ihr hätte Schwer e-n-andri g'funge un sie noch difffciler e-n-andre, un diß isch e famoser Grund, daß zwei enander nemme; alli Hierothe, mache sich so. — Hett nit d’vorig Woch d'r krumm Krämbeschängel d’scheel Nähjermej g’hieroth? jetzt mit ejch isch’s doch e-n-anderlei.

      
"
"
        
Lutz:

        
Wenn ihr mäne, daß ich schunsch kenn Partie meh hätt könne mache, ze trumpeere-n-ihr ejch nit letzi. Ich hätt noch d'Wähler. Erscht vor kurzem isch m’r e Wittfröüj ohne King anoffreert worre. Sie isch m’r awer ze Jungi gewahn.

      
"
"
        
Marx:

        
(lachende)

        
Diß kann m’r vum Tant’l nit säuje, die hett, was se brücht, jeßt diß Schade nix, no hett se-n-äu de Schwooweverstand, un d’Häuptsach for ejch isch, ihr mache-n-e Guten Partie. Wenn ihri paar Haardtstickle äu nemmi veel werth sin un weni Zins ingträuje, so veel i weiß hann’r weni oder gar kenn Vermeje. Lutz Öho, do könnte-n-r awer äu getrumpeert sin. So veel, wie ejri G’schwej wurr i äu noch han, un wenn m’r Rechte löuje wurd, ze wurd sie's sin, wie e Guten Partie macht. Ihr han’s jo ewe selmwer muehn säuje, daß ihri paar Haardtstickle nemmi veel werth sin.

      
"
"
        
Marx:

        
Tant mieux, tant mieux! Es sell mich fräje, daß d’Babette e Guten Partie macht, no brücht se doch nit de Rescht vun ihre Däj im Spital zue- bringe, denn mir hätte se um kenne Preise meh langer gebhalte, denn sie bekummt alle Daö meh Nübbe, in d'r leschte Zitt’’ isch se sogar mit der Hoffahrt geblöujt, m’r sin ere alli nemmi Guten genue angethon. — Was i säuje will, ich will e Sprung nab in de Keller, e Glas Wien hole, for üf ejeri G’sundheit’ anzestoße, denn de brücht m’r, wenn m’r g’hieroth isch, schunsch bringt äne d’r Zom um for d’r Zitt’’. — Mache's ejch zett’r bequemer, Schwöujer 
(ab)
.

      
"
"
        
Lutz :

        
(den Kopf schüttelnd)

        
Schwöujer! . . . Ich weiß nit, die G’schicht will m’r numme noch iwer halwer g'falle, 
(Babette durch die Türe links kommmend, hat den Rat ihres Schwagers befolgt und hat versucht, sich Jungi zu machen; sie ist à la chien gekämmt und ist sehr affektiert.)
 Lutz Was Deifels, wie hesch denn dü dich ge- mutzt? 
(für sich)
 S'isch wid d’r Metzjeri g’säit hett.

      
"
"
        
Babette :

        
(ärgerih)

        
Gemutzt?! — Ich bin im e-n- alter, wie m’r noch ebs üf sin Üsseres leije mueß. — 
(Lutz schüttelt verwundert den Kopf.)
 Ja‚, brüchsch gar nit so dumma ze löuje un ze Lache! — Diß isch iwwerhäupt e-n-appartigi Maneer, sini Huchzytt so ze-n-empfange, un daß de glich wäisch, wenn m’r emol g’hieroth sin, no kummt d’Reihe äu an dich, dü siehsch jo Allewil üs, daß m'r d’r e Sü möcht gen üf d’r Stroß. —

      
"
"
        
Lutz:

        
Oho! — Spar din Sprich! — 
(für sich)
 Jetzt geht m’r e Licht üf wie e Fackel, kenn Wunder, daß se d’r Metzjeri Ios will sin.

      
"
"
        
Babette:

        
Vor alle Sticke muesch emol e Kamifol statt e Blüs träuje in de Woche, un e Kräuje à la mode.

      
"
"
        
Lutz:

        
Schunsch nix?

      
"
"
        
Babette:

        
Ja‚, schunsch nirx! — Doch, noch ebs, wie m’r in d’Stadt kumme, schaffsch d’r e Barrick an

      
"
"
        
Lutz :

        
(verwundert)

        
E Barrick — Ich Glöüwe !? diß Mensch isch maschücke ! — Babette 
(weinerlicher)
! Maschücke !? — Jetzt hör numme-n-än Mensch denne-n-an!

      
"
"
        
Lutz:

        
Ja‚, un daß de glich wäisch, e so kumme- deere loß i mi doch nit vun d’r! 
(für sich)
 S'isch wajer, wie d’r Metzjeri g’säit hett, ich bin noch nit g’hieroth.

      
"
"
        
Babette:

        
Dü bisch meneidi kürios! —

      
"
"
        
Lutz:

        
Was, ich bin kürios?! — Nee, dü bisch kürios, wäifch e so wie dü gitts noch meh; mit dine paar Schelli un dine paar Haardtstickle, wie nix meh träuje, kannsch di nit an de Lade Ieije.

      
"
"
        
Babette:

        
Ja‚, waje was nimmsch mi denn schunsch ?! — Mit mim Vermeje, un so Jungi wie ich noch üffeh, kann ich noch alle Daö äne han, erscht hitt hett m’r min Schwöujer äne anoffreert un d’rzü e Jungier,

      
"
"
        
Lutz:

        
Was Wunders, diner Schwöujer möcht di zuem Huus drüffe han, er hett m’r’s selwer g'säit, du reste, wenn dü Ieewer e Jungi hesch, so nimm denne Jungi, ich könnt äu noch e Jungi han, daß de’ s gradi wäisch!

      
"
"
        
Babette:

        
Doch, die wott ich sahn, wie dich wott mit dim Bluttkopf.

      
"
"
        
Lutz :

        
(im zornig)

        
Was, Bluttkopf?! — S'schient, daß d’r der Bluttkopf doch g’falle hett?! Wer hett denn z’erscht angfröuit, ich oder dü ? Wer isch d'r Ammej fascht s' Huus inggeloffe?

      
"
"
        
Babette:

        
Wer? — Dü !! —

      
"
"
        
Lutz :

        
Nee, diß Toupet ! — Ich hab langer genäuej nit gewellt, kannsch d’Ammej selwer fröuje.

      
"
"
        
Babette:

        
Dü Lejier! — Ich hab nit gewellt, wenn d’Ammej nit gewahn wär, wie hett g’säit, ich mach e chrischtlichs Werik, wenn ich dich nimm, wie de so verwahrlost erumgehsch, ze hätt i mich wajer nit dessideert! Lutz 
(im größten zorn)
 Wie die alt' Schachtel d’Sache verdräje kann! Babette 
(sinkt Halb ohmmächtig in den Lehnstuhl)
 Was? Alti Schachtel, ich wurr ohnmächtig 
(ste trocknet sich ab mit dem Taschentuch)
 Schwöujer ! Schwöujer ! Wasser! Effi! Alti Schachtel hett'r m’r g’säit! Alti Schachtel!

      
"
"
        
Lutz :

        
(für sich)

        
Ich hab’ D’Ammej gemänt, awer ’s magıt äu nix, deschto besser! — Marx 
(von links kommend)
 : Um s’Himmelswille, was isch denn do los?

      
"
"
        
Babette :

        
(richtet sich auf)

        
Alti Schachtel hett’r m’r g'säit ! — Schmisse ne nüs denne Gascht ! — Oh, ich wurr ohnmächtig 
(sinkt wieder in den Lehnstuhl zurück)
.

      
"
"
        
Marx :

        
(zu der Türe hinausrufend)

        
Fräu ! g’schwind Essi ! 
(Zu Lutz)
: Was isch do vorgange, jetzt bring i se glauw’ wid’r nit los, was m’r diß nit isch ! — Nemme se doch, ich will die Sach' wid’r arrangeere !

      
"
"
        
Lutz :

        
(langt schnalle seinen Hut und läuft zur Türe hinten)

        
Nee, nee, um s’Himmelswille nee, b’halte-n-er se numme, ich will se um kenne Preise, d’Äuje sin m’r uffgange ! 
(Babette ist unterdessen wieder zu sich gekommen und hört den Schluß der unde- haltung)
. E so e Gifthaafe? Nee, merci veelmol ! merci!

      
"
"
        
Babette:

        
Was!? Gifthaafe ! Ah! 
(erneute Oh: machtsanfall)
.

      
"
"
        
Lutz :

        
(für sich)

        
Jetzt isch Zytt, daß i d' Platt buetze ! 
(Zu Marx)
: Adjeh, un nix for unguet! 
(schnalle ab)
.

      
"
"
        
Frau Marx:

        
(von links kommend mit Essig)

        
O dü meinenen! O dü meinenen! Was isch denn g’schehn? 
(macht Babette den Effig schnupfen, dieselbe kommt zu sich)
.

      
"
"
        
Babette:

        
Schachtel! Bifthaafe! 
(versteckt ihr G'sicht in den Händen)
. Der impertinent Mensch! 
(Zu Marx Schwöujer)
 gelte-n-ihr verzeihj m’r, was i gemachti hab? — Ich wil’s secher nimmi thuen!

      
"
"
        
Marx:

        
Ja‚ m’r verzeihj-n-i, un sin froh, daß’r die alt Schwardt nit bekomm han! Ich suche ejch jetzt e Jungi.

      
"
"
        
Babette:

        
Nee! Nee! D’Mænner sin alli kenn Bohn werth ! — Ich bin küreert, ich will kenne meh, um kenne Preise! — gelte-n-ihr b’halte mi wid’r ?

      
"
"
        
Frau Marx:

        
Ja‚ gewiß, dü derfsch wid’r bie uns bliewe!

      
"
"
        
Babette:

        
Un tröschde-n-ejch jetzt, un vergasse de mankeert Hieroth!

        
Der Vorhang fällt

      
"
