"
        
Schuster:

        
Hört m'r ebs?

      
"
"
        
Marie:

        
Nix, gar nix.

      
"
"
        
Schuster:

        
Isch's möjli, daß so e gottvergessener Mensch, wie sini Frau umgebrocht hett, noch so ruehij schlofe kann!

      
"
"
        
alter Frau:

        
Ja‚, sin’r ejrer Sach) denn au ganz secher ?

      
"
"
        
Schuster:

        
Au noch?! Ihr muehn e solide Schlof han oder daub sin wie e Pfohl, wenn er nix g'hört han vun dem Möerder un Totschlaa 
(zu den Leuten im Türrahmen)
 Ihr han’s doch alli ghört? —

      
"
"
        
Alle :

        
(mit dem Kopfe nickend)

        
Ja‚, un ob!—

      
"
"
        
Katherin:

        
Es isch ganz gottsjämmerlich g’sin, diß Gebruels anzehöre ! Die arm Frau hett gejehlt un gekrische, wie er als üf se g’haue hett, daß sich d'Stein hätte-n-erweiche möchte!

      
"
"
        
Schuster:

        
E jede Streich hawi g’hört, wie ere versetzt hett.

      
"
"
        
alter Frau:

        
Um well Zitt’ isch’s g’sin?

      
"
"
        
Marie:

        
S’isch geje de Zwölfe g’sin.

      
"
"
        
alter Frau:

        
In d'r Beischterstund ?!

      
"
"
        
Marie:

        
Ich hab gradi getraimt g’hett, daß d’Hüsare vun Stroßburri versetzt worre sin, do krej ich e Mordsschrecke un verwach un hör im Zimmer newe mir — wisse Sie, ich schloof newe d’r Herr- schaft wäje-n-em Kläiner Kind — un hör, wie g’saat, e Heidespektakel.

      
"
"
        
Schuster:

        
Grad wie ich, ich hab gradi getraimt, ich hab e Los genumme in ere Lotterie, un im Moment, wie ich m’r’s Nummero hab merike welle, wur ich uffgeweckt durich e Läwesdaa iwer mir, wie ganz gottvergesse-n-isch g’sin. Ich rieb m’r d’Aue-n- üs un fall fascht in ohnmächtig, im Bett newe mir steht e großi wiffi G'stalt ufirichte an d'r Wand...

      
"
"
        
alter Frau:

        
Um’s Gottswille isch’s möjli? — Es isch doch ken Gspänscht g’sin?

      
"
"
        
Schuster:

        
Nein, es isch mini Frau g’sin, wie au schun vun dem Läwesdaa uffgeweckt isch g’sin, un wie gelüschtert hett. „Hörsch?“ sät sie zue mir. „Ja‚ Frau,” hawi zue ere g’sät. „Diß isch jo verschreck,” hett sie g’sät, „diß isch jo verschreck” saa ich d’rno.

      
"
"
        
Katherin :

        
(welche bestätigend mit dem Kopfe genickt hat)

        
Un no han m’r mit-nander gelüschtert. D’hoor sin uns ze Berry g’stande, wie der sini Frau verschlaaue hett. Bim e jede Streich wie m’r süse han höre, han m’r uns als gebuckt, un glich druff hett m’r als wid’r krische höre. Diß Ding hett e Wyl so furt gedürt, bis’r sie kälter gemachti hett g’hett. Im ganze Zimmer isch’r ’re nochgeloffe, bis’r se verwitscht hett. Sie hett sich natierlich flüchte welle, awer der Eländer Herr hett allewäj d’Tür zueb’schlosse g’hett —.

      
"
"
        
Schuster:

        
„Un dü gehsch m’r doch nit durich,” hett’r als g’saat, „miserawls G'schöpf, elends,” un bumms hett'r wid’r eins drufflosg’haue, un die arm Frau hett gekrische, daß’s eim durich Mark un Bein g'fahre-n-isch.

      
"
"
        
Katherin:

        
Do üf einmol hett m'r noch e starick Streich g’hört, d’rno hett m’r krische höre, wie ebber, wo e Meffer im Hals stecke hett.. . .

      
"
"
        
Schuster:

        
Un d'r Möerder, haw ich g’hört, wie'r ganz ditlich g’saat hett: „So jetzt hesch furs Murre —”

      
"
"
        
Katherin:

        
So isch's, ja so isch's!

      
"
"
        
Marie:

        
Un no isch’s stilli worre.

      
"
"
        
Katherin:

        
stilli, wie im Grab! —

      
"
"
        
alter Frau:

        
Diß isch jo grieserli ! Nein, so e Canaille ! Marie 
(an der Tür durch das Schlüsselloch schauend)
 : Jesses Maria, jetzt kummt’r allewäj glich, er thuet gradi d’Hosse-n-an.

      
"
"
        
Katherin:

        
So ganz kaltbluetig?! So mir nix dir nix ?! — O, die Canaille! S’bescht wurd sin, ich geh jetzt die arm Mamme prevenire, die wurd lueije, wenn ich's ere so hinte-n-erum ze verstehn wurr gen, daß se-n-e Möerder zuem Dochtermann hett.

      
"
"
        
alter Frau:

        
M’r grüst’s ganz, wenn’r jetzt awer so üf einmol erüs thät kumme.

      
"
"
        
Schuster:

        
Nur kenn Angst ! Do solle Sie ebs erlewe, do solle Sie emol sehn, wie ich denne anne- stella wurr. Sie gottvergessener Frauemörder, wurr ich zuem saaue un wurr'm d’rbi in’s Wiß vun sine-n- Aue löuije, daß'r for Scham in de Erdsgrundsbodde nin versinke möcht!

      
"
"
        
Marie:

        
Scht! — Ich Glöüwe er kummt 
(die Türe geht auf, Herr Müller tritt herein, er hat eine Kläiner Schachtel in der Hand und sieht erstaunt um sich. Marie reibt hastig mit einem Lappen an einem Möbel herum; die Aelterer Frau, Katherin und die Leute im Türrahmen stieben entsetzt davon, Schuster hinterdrein, Müller eilt ihm nach und halte ihn zurück.)

      
"
"
        
Müller:

        
Schueschter, Schueschter! Ja‚, was Deifels, was isch, denn do Ios, ich Glöüwe ihr han Conferenze do in mim Zimmer in aller Fruehj? 
(Schuster in allen Ängsten sucht nach einer Antwort)
 Ze redde doch, was han’r denn gewellt ?!

      
"
"
        
Marie :

        
(für sich)

        
Schreckli ! Wie verschreck daß’r Uessicht!

      
"
"
        
Schuster:

        
S'isch nix, gar nix, Herr Müller, gar nix, ich hab ne numme d’Zyttung eruff gebrocht 
(sucht in seiner Schürze)
.

      
"
"
        
Müller:

        
Ja‚, un die viele Lytt? —

      
"
"
        
Schuster:

        
Ei, — ei, die han m’r sie hälfe bringe...

      
"
"
        
Müller:

        
Ja‚, un do d’rzue brücht m’r e so viel Lytt ?

      
"
"
        
Schuster:

        
Diß heißt, sie han mich begleiten, sie han gradi Guten d'r Zitt’ g’hett . . . . sie sin spaziere gange.

      
"
"
        
Müller:

        
(für sich)

        
Ich Glöüwe der Mænner hett de Vrstand verloren, 
(zu Schuster)
 ze gen m’r se e Gottsname here die Zyttung.

      
"
"
        
Schuster:

        
Grad suche i sie, jetzt sieh i ererscht, daß i se vergeffe hab... Ich will sie glich gehn hole.

      
"
"
        
Müller:

        
Ich weiß gar nit, wie Ihr mir vorkumme, m’r meint, Ihr han glauw’ schun e Dampf

      
"
"
        
Schuster:

        
(in der Mitteltür)

        
Besser e Dampf, wie e schlechts 'Gewisse schleunig 
(ab)
.

      
"
"
        
Müller:

        
(den Kopf schüttelnd)

        
Total verhuttelt, total verhuttelt! Ex dürt mi, der arm Mænner. —

      
"
"
        
Marie:

        
Ich rieder an Arm und Bein, allei bliew ich au net 
(will ab)
.

      
"
"
        
Müller:

        
Worum lauft Sie denn so furt wie nasebluetig ? Sie hätte g'scheit dran gethon, wenn Sie mir de Kaffee ferviert hätte, als do ze rätsche; un daß m’r diß nimm vorkummt, daß i fremdi Lytt am Morjen in minere Wohnung treff.

      
"
"
        
Marie:

        
Ah, de Kaffee! — Ja‚, Herr, im Aues- blick 
(schnalle ab)
.

      
"
"
        
Müller:

        
Ich weiß nit, die Sach kummt m’r so merkwürdi vor, was do nur for e Gheimnis drunter stecke mueß. C’est curieux, très curieux!

      
"
"
        
Jeanne :

        
(aus dem Schlafzimmer)

        
Adolph!

      
"
"
        
Müller:

        
Ma chérie? —

      
"
"
        
Jeanne:

        
Vergiß jo nit die Fledermüs üsstopfe ze Ion.

      
"
"
        
Müller:

        
Ja‚, ewige Frau, ich hab schun dran gedenkt, ich hab sie do in ere Lad.

      
"
"
        
Jeanne:

        
M'r schenke sie no im Unkel for in sin Natüraliekabinet.

      
"
"
        
Müller:

        
Oui, Herzele. 
(Zieht eine tote Fledermaus aus der Schachtel, dieselbe mitleidig betrachtend)
 Arm’s Thier’l, so hesch dü din Läwe-n-inbüße muehn ! Diß hesch dü dier bezahlt, daß dü in dere warme Sommernacht zuem offener Fenschter in unser Schlofzimmer ning’flöuje besch, un daß dü min liebs herzigs Wiewele so ver- schreckt hesch. 
(Legt die Fledermaus wieder in die Schachtel)
. S'isch einthuen, ich möcht nit alli Nacht so e surprise han. Was diß e Hitzig koscht het, bis ich diß arm Thierl verwitscht hab. Huppla, wie ich als gemeint hab, jetz triff i’s mit'm Stöcke, isch’s schun wittersch g'sin und ich hab d’rnewe g’schlaue, üf’s Bett, üf’s Commod, an d’Wand. Diß isch e fuerichterlicher Krambol g’sin, d’rno hett mini Frau jedesmol gekrische, wenn d’Fledermüs iwer’s Bett g'flueje-n-isch, daß m’r hett meinen könne, es geht ere-n-an de Kraue. Ich fuehricht fascht m’r han ’s ganz Huus rewellisch gemachti. Zum große Glüeck hawich sie finalement doch verwitscht. En plein hawich ich sie getroffe, un tot isch tot g’sin, un kapüttt kapüttt! 
(bei den Ietzten Woorte ist die Magd in der Mitteltüre erschienen, vol Entsetzen hört sie die Ietzten Woorte. Sie fängt an zu zittern und läßt das Plateau falla, mit Caffe, Kaffee und Milchkanne)
.

      
"
"
        
Marie:

        
Kapüt isch kapütt!

      
"
"
        
Müller :

        
(sich entsetzt umwendend)

        
Ei, d’r Schinder, was macht sie do schun wid’r, Sie verhejt uns jo noch alles G’schirr. 
(auf die Magd zu Daß Sie doch nie Acht gen kann! — Marie zitternd)
: Verschone Sie mi, Herr, ver- schone Sie mi !

      
"
"
        
Müller:

        
Macht Sie kenn so dumma Plan, ich will Sie jo nit fresse, un paßt Sie mir e-n-andrs Mol besser üf. 
(Es schlägt 8 Ühre, Müller schaut auf die Ühre
. Bon, jetzt kumme i au noch ze spoot üfs Bureau mit dere-n-Affär, un von Kaffeetrinke kann kenn Red meh sin. 
(Zu Marie)
 Hol Sie Kehrwisch un Bese, un fäj Sie mir d’Scherwe zamme. ’s isch nimm üszehalte mit dem Dienschtpersonal. Ieanne 
(aus dem Nebenzimmar)
: Mannele, gäll's Marie hett wid’r ebs verhejt?

      
"
"
        
Müller:

        
Ja‚, ’s Kaffeeservice.

      
"
"
        
Jeanne:

        
Do will ich awer g’schwind uffstehn.

      
"
"
        
Müller:

        
Nein, liebs Kind, blieb nur noch Ieje, dü hesch bös genue g’schloofe die Nacht wäje dere Fledermüs . ... Au revoir, Schätzele, ich mueß uffs Bureau gehn.

      
"
"
        
Jeanne:

        
Et tu n’embrasses pas ta petite femme avant de partir?

      
"
"
        
Müller:

        
Si chérie! 
(ab ius Schlafzimmer)
.

      
"
"
        
Marie :

        
(mit Besen und Schaufel)

        
Schreckli, fchreckli, ich zit’r am ganze Köerper.

      
"
"
        
Müller:

        
(zuräck, nimmt die Kläiner Schachtel mit der Fledermaus)

        
’s Üsstopfe derfe m’r nit vergasse ! — 
(Zu Marie)
 Un paßt Sie mir e-n-andrs Mol besser üf ! 
(ab)
.

      
"
"
        
Marie:

        
Ja‚ Herr! — 
(für sich)
 Gott sei Danke, daß’r furt isch. Daß au gradi mir diß Ungleck passiere mueß, in e Huus ze kumme, wo e Mænner sini Frau umbringt! O, wär ich nie gebore ! Schuster 
(den Kopf vorsichtig zur Mitteltüre herein steckend)
 : Er isch furt ! Er isch furt! 
(Marie dreht fisich ersechreckt um)
 Ich hab awer d’Bolizej preveniert ! Sie wurd bald kumme ! 
(reibt sich die Hände)
.

      
"
"
        
Marie:

        
D’Bolizej?! — O, es grüst m’r ganz.

      
"
"
        
Schuster:

        
Hein? Mamsell Marie, han Sie ne Rechte bedrachte, wie verstört, daß’r üsg’sehn hett, wie bleiche daß'r isch g'sin? Un wier verschreck-n-isch, wie ich'm eine niewer g’stellt hab, un wie ich’m g'saat hab, daß’s besser isch, m’r hett e Dampf in d'r Fruehj als e schlechts 'Gewisse !

      
"
"
        
Marie:

        
Ja‚, er isch ganz zammeg’fahre !

      
"
"
        
Schuster:

        
Han Sie's au gemerikt, wie'r mim blick üsgewiche-n-isch, wenn ich ne als angeluejt hab? 
(Marie nickt mit dem Kopf)
. Sehne Sie, Mamsell Marie, diß macht im e-n-Ehrli Mænner Fräid, wenn’r im e Ver- brecher so Rechte in d'Aue Iueje kann, um sin 'Gewisse schlaaue ze mache. Marie 
(an der Türe links lauschend)
? Jesses, m’r sott fascht meinen, m’r hört ebs!? — Jesses Maria!

      
"
"
        
Schuster:

        
Sott se-n-am End noch nit ganz tot sin ? — die arm Frau, sie thät mich düre, wenn se liede mueßt. 
(Die Mitteltüre geht auf, Madamen Schmidt, das Taschen- tuch in der Hand, tritt ein mit Katherin, der Frau des Pförtners)
; Do kummt die arm Mamme ! —

      
"
"
        
Katherin:

        
kumme se numme-n-rin, Madamen Schmidt.

      
"
"
        
Mme. Schmidt :

        
(trostlos mit dem Kopf hin und her)

        
O‚ ich arm unglicklichi Frau !

      
"
"
        
Schuster:

        
Ich nemm Antheil, Rechte Antheil an ihrem große-n-Ungleck.

      
"
"
        
Mme. Schmidt:

        
O, ich arm, arm Mamme ! Ich kann’s als noch nit Rechte Glöüwe, isch’s denn au ganz secher wohrer?

      
"
"
        
Schuster:

        
Un ebs wohrerer isch?! — Diß wär noch Schön, wenn's nit wohrerer wär, ’s isch grieserlich g’sin — grieserlich ! Es hett hatt e so kumme muehn.

      
"
"
        
Katherin:

        
Ja‚, es hett so kumme muehn!

      
"
"
        
Schuster:

        
Wenn m’r’s bedenkt, ze-n-isch’s schun ewyl, daß die Sach dürt. Bal alle-n-Öwe hett’r se so verschlaaue. Es isch gottsjämmerli g’sin, küm zuem anhöre.

      
"
"
        
Mme. Schmidt:

        
Ja‚, un worum sin se-s-ere denn nit ze Hilf kumme, wo sie so gekrische hett?

      
"
"
        
Schuster:

        
Ja‚, was soll ich do saue. Wenn einer sini Frau durichhaut, diß isch Privatsach, un in Privatsahe melier ich mich als Concierge par prineipe nit. Mme. Schmidt 
(geht nach der Türe Iinks)
 : Die arm Doochder, es grüst m’r ganz, in’s Schloofzimmer ze gehn 
(macht die Türe auf und tritt wieder zurück)
 Jeanne ! Jeanne !

      
"
"
        
Jeanne:

        
Bisch dü's Mamme?

      
"
"
        
Mme. Schmidt:

        
Ja‚, liebs Kind. 
(Aufathmend. Zu den anderen)
 : Sie lebt noch! Sie lebt noch! Schuster 
(enttäuscht)
 : Sie lebt noch ?!

      
"
"
        
Jeanne:

        
Ich kumme gradi, Mamme.

      
"
"
        
Schuster:

        
Er hett se-n-also nur durichg'haue, diß isch jetzt ambetant.

      
"
"
        
Katherin:

        
Die mueß awer e zäh’s Lewe han.

      
"
"
        
Jeanne:

        
(im Négligé auf die Mama zu und sie umarmend)

        
Guten Morjen Mamme. (Sieht mit großer Verwunderung die anderen, welche ganz zusammengeknickt dastehen. Besorgt: Ja‚, was isch denn? Was han’r denn, daß’r mich alli so anlöuje?

      
"
"
        
Mme. Schmidt :

        
(die Arme ausbreitend)

        
Jeanne, mini Doochder, mini arm ungluecklicher Doochder!

      
"
"
        
Jeanne:

        
Mamme, Mamme ! 
(fällt ihr Weinend in die Arme)
 Ja‚, was isch denn, liewi Mamme? —

      
"
"
        
Mme. Schmidt:

        
Arms Kind, hawich dich wäje dem verhierothe muehn?

      
"
"
        
Jeanne:

        
Ja‚, redd doch Mamme, gell mim Mænner isch e-n-Ungleck zueg’stoße ?

      
"
"
        
Mme. Schmidt:

        
Wenn's nur diß wär !? — Ich versteh dich nit, wie kannsch dü dich nur so verstelle, wie wenn de vun nix wisse thätsch. Wie kannsch dü nur noch nooch dim Mænner fröuje ? — So e Monstre, so e schlechten Mensch ! So e Galleefueder !

      
"
"
        
Jeanne:

        
O, jetzt versteh ich Alles! Alles! N’est-ce pas, il me trompe? Er hintergeht mich?! Oh ich arm Frau! 
(umarmt abermals die Mutter)
. Müller 
(hastig zur Mitteltüre herein, sehr verwundert)
 : Ja‚, was isch denn do los? — Schuster, Katherin, Marie 
(entsetzt)
: Uh ! — do kummt’r wid’r ! — Mme, Schmidt 
(theatralisch ihre Doochder beschützend)
; Ah do kummt der suufer ,patron...!

      
"
"
        
Müller:

        
Ja‚, zuem Gücksel, was isch denn do los? Was mache Sie denn mit minere Frau?

      
"
"
        
Mme. Schmidt:

        
Ich b’schuetz mini Doochder, wie e Glüeck ihri Huehnle, wenn d'r Raubvöjel kummt.

      
"
"
        
Müller:

        
Ja‚ zum Henker, isch denn hytt Alles verrückten? — Jeanne 
(weinerlicher)
 : Brüchsch dich nit ze verstelle, ich weiß Alles ! Alles !

      
"
"
        
Müller:

        
Alles weisch? Ja‚, was Alles?

      
"
"
        
Schuster :

        
(für sich)

        
Der hett e toupet. 
(Zu Herrn Müller)
 : Ei, elle sät tout, sie weiß Alles!

      
"
"
        
Müller:

        
Diß isch jo rein zuem verschlenze !

      
"
"
        
Jeanne:

        
Min Mamme hett m’r alles g’saat 
(Weinend)
 Ja‚, diß hätt ich nie vun d’r geglauibt, daß dü dich mit andere-n-abgiebsch, un dini Frau so hintergehsch.

      
"
"
        
Müller :

        
(wüthend)

        
Na, jetzt saau ich au nix meh. Was hett dir dinni Mamme do g’saat? 
(Zu Madamen Schmidt)
: Was traaue Sie do ihrer Doochder for Mähre zue, um Unfridde in unseri Hüshaltung ze bringe. Ich fröuj mich, worum daß d’Schöpfung d’Schwejer- muedre erschaffe hett ! — S'isch eini so viel werth wie d’ander. 
(Auf und ab.)

      
"
"
        
Schuster :

        
(für sich)

        
Wenn i numme durich d’Latte könnt.

      
"
"
        
Mme. Schmidt:

        
Wenn d’Schwejermuedere nix werth sin, no sin awer Dochtermänner erscht Rechte nix nutz !

      
"
"
        
Müller:

        
Ja‚, in’s drei Deifels Namme, ze saaue Sie jetz doch emol, was Sie mit uns welle?

      
"
"
        
Mme, Schmidt:

        
Was i will?! Mini Doochder wid’r zue m’r nemme.

      
"
"
        
Schuster :

        
(für sich)

        
Bravo! — A la bonheur!

      
"
"
        
Müller:

        
Ich Glöüwe Ihne rappelt’s?

      
"
"
        
Mme. Schmidt:

        
Diß Ding kann nimm so furt- gehn, meinen Sie denn, daß ich Ihne mini Doochder gen hab, for daß Sie se-n-alli Nacht durichhauwe wie e Tanzbär, so daß s’ganz Huus rewellisch wurd.

      
"
"
        
Müller:

        
Was fawle Sie do?

      
"
"
        
Schuster :

        
(für sich)

        
Er Ieigelt, diß isch e secher Beweise.

      
"
"
        
Jeanne:

        
Was saasch dü do, Mamme ? Miner Mænner haut mich wie e Tanzbär? — Ja‚, wer bind denn dir so Bär-n-an?

      
"
"
        
Mme. Schmidt:

        
Verstell dich au noch, brüchsch au noch, zuem ze halte,

      
"
"
        
Jeanne:

        
Jetzt versteh ich schun gar nix meh.

      
"
"
        
Müller:

        
Diß isch for mich spanisch.

      
"
"
        
Mme. Schmidt:

        
’s wurd am End nit wohrer sin, daß dich diner Mænner die Nacht am zwölfe g’haaue hett, un daß de e so gekrische hesch, daß m’r's im ganze Huus g’hört hett, un daß d’Lytt gemeint han, s'isch Möerder un Totschlaa, un daß m’r même d’Boli- zej preveniert hett.

      
"
"
        
Schuster:

        
Ja‚, sie kann alle-n-Auesblick kumme.

      
"
"
        
Jeanne :

        
(Fräid)

        
Mamme, diß isch gelöuje, ganz gewiß gelöuje ! 
(Müller lachende den Kopf schättelnd.)

      
"
"
        
Mme. Schmidt:

        
Was gelöuje? D’Noochberslytt han dich um Hilf ruefe höre, sie han’s au ganz ditlich g’hört, wie finalement diner Mænner g’saat hett: So jetzt hesch for’s Murre ! —

      
"
"
        
Schuster:

        
Oui c’est ça!

      
"
"
        
Müller :

        
(lachende)

        
Aha! — Wittersch im Text !

      
"
"
        
Mme. Schmidt:

        
D’ganz Stadt isch jo rewellisch, iwwerall heißt's, daß dich diner Mænner totg’schlaaue hett.

      
"
"
        
Jeanne:

        
Ja‚ Mamme, wäje dem bisch kumme? — S'isch nit g’sin for mir ze saaue, daß mich meinen Mænner hintergeht? —

      
"
"
        
Mme. Schmidt:

        
Von dem weiß i nix, ich bin nur kumme, um d'rgeje ze protestire, daß’r dich alli Nacht verhaut ! 
(Jeanne und Müller brache in lautes Lache aus.)
 Ja‚, was Lache-n-r denn jetzt so dumma? — 
(Jeanne und ihr Mænner Lachen aufs neue)
 Diß soll jetzt nix sin ! —

      
"
"
        
Schuster:

        
D’r V’rstand steht m’r stilli ! 
(Katherin und Marie schütteln den Kopf.)

      
"
"
        
Mme. Schmidt:

        
Ja‚, was han'r denn ?

      
"
"
        
Jeanne:

        
Ei liewi Mamme, diß isch e großes Mißverständnis.

      
"
"
        
Müller:

        
Es handelt sich üf jede Fal um nix als um e Fledermüs, wie in unser Schloofzimmer g'flöuje-n-isch, un wie ich alli Müehj hab g’hett for ze Fange?

      
"
"
        
Mme. Schmidt:

        
Ja‚ un diß Gekrisch ?

      
"
"
        
Müller:

        
C’est bien simple, jedes Mol, wie d’Fledermüs iwer’s Bett g’flöuje-n-isch, hett’s Jeanne e Gäll üsgelon — S’nettscht isch, daß ich wäje dere G'schicht üs’m Bureau heimkumme bin, au dort ver- zehl se schun, daß in unserer Stroß e Mænner sini Frau un sini Kinder umgebrocht hett.

      
"
"
        
Jeanne :

        
Do isch üs der Fledermüs e-n-Elephant gemachti worre. Adolph, Liawe Adolph, gell dü ver- zejsch m’r, daß i numme-e-n-Auesblick an d’r zwiefle hab könne ?!

      
"
"
        
Müller:

        
D’Hauptsach isch, daß Alles nit wohrerer isch un nie wohrerer wäre wurd. 
(Sie umarmen sich.)

      
"
"
        
Katherin:

        
Diß isch e dumma Stoß.

      
"
"
        
Schuster:

        
S’isch doch ambetant, wenn doch numme wenigstens e bissl ebs an d’r Sach wär g'sin. Hoffent- lich kummt’s noch, ganz suufer schient’r m’r doch nit ze sin.

      
"
"
        
Mme. Schmidt:

        
Mon gendre, verzeihj Sie, s’isch jo nit mini Schulde, umarmt Müller).

      
"
"
        
Müller:

        
Ich nemm gern zeruck, was i iwer d’Schwejermuedre g’saat hab, d’Schwejermuedre solle Iewe !

      
"
"
        
Mme. Schmidt:

        
Un d’Dochtermänner d'rnewe ! 
(Zwei Polizischt tratte herein)
.

      
"
"
        
Marie:

        
Uh ! do kummt jetzt d’Bolizej ! 
(Alle drehen sich verwundert um.)

      
"
"
        
Schuster:

        
Ihr liewi Herr vun d'r Bolizej, ihr muehn excusiere, ihr kumme awer, wie Allewil, ze spoot.
Der Vorhang fällt.

      
"
