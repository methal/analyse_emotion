"
          
Jacques:

          
Jaja, Madamenenenenenenen. — 's isch Guten, Madamenenenenenenen! — 's wurd bsorjt, Madamenenenenenenen!

          
(zum Publikum):

          
Wenn unseri Madamenenenenenenen bekomm däet was ich ere Winsch, no müeßt se emol vor alle Dinge jede Daa e Kilo
          zünämme, bis se am e schen Daa v'rknelle däet wie e gebrooteni Bluetwurscht. Die Hawwergais, die
          Häriseel, die Linsespaltere und Pfennifuchsere, die — — die giftiger Krott, die miserawel! — Pardon! —
          Wenn ich mich v'rzürne wott, ze däet ich ere noch ebs ganz anderschs v'rzähle! Aw'r naan, ich will mir
          mini Ruehj reserviere. Nit daß ich mir wäeje Dere 's Läwe Sire mach! — 's falt m'r in! — So ebs daet
          ere jo basse! — Denn üf dies geht se üs, Daa fer Daa. kann ich ebs d'rfür, daß ich numme valet de
          chambre bin?! - kann sie ebs d'rfür, daß sie Madamenenenenenenen isch worre?! — Worum isch's nit umgekehrt?! — Worum
          ich sie nit valet de chambre und ich d'Madamenenenenenenen?!-

          
(Es klingelt.)

          
Aha! — Sie schellt wid'r emol e Düür, unseri Madamenenenenenenen ! — Die lebt jo nit ruewie, wenn se merkt, daß
          Unseraans emol e bissel schnüüfe köent.

          
(Es klingelt.)(wü-

        
"
"
          
Marie

          
(bedauernd):

          
Oh, dies schen Rösele. Gaischt noch so natt geblüejt, und hyt schun welk! — 
          
(seufzt tief auf.)
 — O ja, so isch's halte emol üf d'r Welt! — So geht's mit de Rose — so
          geht's mit de Maidl. M'r blüehjt — m'r wurd umschwärmt — m'r v'rwelkt — un wurd gemiede! 
          
(Starkes Geklirr hinter der Bühne.)

        
"
"
          
Marie

          
(erschreekt):

          
Uh! Do gitt's Schärwe?!

        
"
"
          
Jacques

          

          
(hinter der Scene):
 E Millonezunderschtzewwerst soll d'rzwische nin bängle, aans vun unde
          un aans vun owwe — wenn's will!!!

        
"
"
          
Marie

          
(lachende):

          
Aha, 'skracht in d'r Kiche, no wurd's au bal rauche! 
          
(Es klingelt.)

        
"
"
          
Marie

          
(lachende):

          
Hoppla! — Was hawich nit gsaat? — 's raucht schun! 
          
(Es klingelt.)

        
"
"
          
Jacques

          
(hinter der Scene.:

          
Jo riß d'Schell vun d'r Wand erab, alter Nißkraawe! 
          
(Kommt hastig von rechts.)

        
"
"
          
Marie

          
(lachende):

          
Oho, Jacques, so böes gelüünt?

        
"
"
          
Jacques

          
(Wüat):

          
Wenn ich nin kumme, ze riß ich ere diesmol d'Barriek vum Schäddel, daß se au emol weiß, was schelle
          heißt! 
          
(eilt ufgeregt nach links ab.)

        
"
"
          
Marie

          
(lachende):

          
's isch nit halwer eso schlimm. Wenn'r nin kummt, no isch wied'r d'Madamenen hinte un d'Madamenen vorne. —
          Ja‚, d'r Ja‚cques! — Hahaha! Er isch e Dschori! — E Gutenmuethiger Dschorit -E aanfältiger Dschori! — Er
          het's gradi wie die Hund, wie erscht's Härze han ze belle, wenn sie innewendi hinterem Guten v'rrejelte
          Hofdärel stehn! — Haha! — Mit so eme Mænner wär Guten lab. — So aaner dhät sich zehje loon, so wie m'r
          'ne gern hätt. — Alt genuej zum hyroothe wär 'r juschtemend, un ich glaab küm, daß ich's 'm abschlaawe
          dhäet! — Er het e paar Fränkle üf d'r SparkaB, ich hab min Gedüech un e Bettlad — brücht m'r sunscht
          noch vielerlei mehr fer de anfang? — Was noch fahlt, schafft m'r sich so v'rplämpelterwies an. — E
          Hauptbedingung isch numme: m'r mueß z'halb Zweit sin zem hyroothe, un — — — ım'r mueß enander welle. — 
          
(setzt sich auf die Bank.)
 — M'r mueß enander welle?! — 
          
(seufzt tief auf.)
 — Wie wenn ich de Ja‚cques nit wott?! — O Gott, o Gott, o Gott! 
          
(Ja‚cques tritt unbemerkt von links ein.)

        
"
"
          
Marie

          
(seufzt):

          
Ich gieb's 'm jo jede Daa dytli genüej ze v'rstehn, aw'r er merkt nix! — Er merkt nix!

        
"
"
          
Jacques:

          
Wer merkt nix?

        
"
"
          
Marie

          
(springt mit einem erschreekt Aufschrei in die Hôhe.)

          
Aw'r Jacques, wie kann m'r numme eso schlich?!

        
"
"
          
Jacques

          
(erstaunt):

          
Schliehe? — Un d'rbie sät d'Madamen alle gelte zue m'r: ich däet dappe wie e Elephant?!

        
"
"
          
Marie

          
(lachende):

          
Die het jo ihrer Läwe noch gar kaan Elephant gsehne! — Sie renomiert halte gern mit ihre
          astronomische Kenntnisse.

        
"
"
          
Jacques:

          
Aw'r numme fer mir 's Läwe Sire ze mache. — Sie kann mich halte nit schmecke. — Ich sie aw'r au
          nit!

        
"
"
          
Marie:

          
Rej Dich doch nit üf, Jacques. — Het se 's mit mir nit 's nämli? Dhuet se mich nit au d'halb Zyt
          ärjere un küjoniere, daß ich grüen un gääl könnt wäre vor Plaisir? — M'r mueß aw'r Allewil bedenke:
          wenn d'r Mensch ins alter kummt, no wurd'r wunderlich. — Unseri Madamen — — —

        
"
"
          
Jacques:

          
isch ewwe e-n-alts Mensch!

        
"
"
          
Marie:

          
Aw'r Jacques!

        
"
"
          
Jacques:

          
Ei na, sie isch doch kaan Stück Vieh?

        
"
"
          
Marie:

          
Geh, schäm Dich doch e bissel, Jacqes! Ich hab ewwe numme welle saawe: unseri Madamen het ihri Lünne
          zyttwies un randswies, wie dies ewwe 's alter eso mit sich bringt. — Wenn se aw'r d'rnoo ihri
          ""Rootschläej"" üf uns los renne loßt, no loon mir se ewwe ze aam Ohreläppel nin un zem andere wied'r
          nüs schnärre. — Schmerzlos!

        
"
"
          
Jacques

          
(kraut sich hinter den Ohren):

          
Jaja, 's isch alles Rechte, un ich will unserer Madamen au nix schläechts Winsch — - behüet selle m'r
          sin — aw'r Plaisir dhäet's m'r halte mache, wenn ere emol e Elephant mit glichlinge Füeß üf de Mauue
          hüpse däet! — — —

        
"
"
          
Marie

          
(lachende):

          
Aw'r Jacques!

        
"
"
          
Jacques:

          
Numme daß se 's inne dhäet wäre, was unde eme Elephant v'rsteht, der alt Pomad- hafe!

        
"
"
          
Marie

          
(sich räuspernd):

          
Höer Jacques — jetz — jetz will ich — jetz will ich Dich ebs fröije.

        
"
"
          
Ja‚cques:

          
Ja‚? — Was denn?

        
"
"
          
Marie:

          
Jacques, Dü wit doch ganz secher nit diner Läwelang d'Füeß unde fremdi Lyt ihrer Disch strecke?

        
"
"
          
Jacques:

          
Naan, ganz secher nit.

        
"
"
          
Marie:

          
Un speziell do in dem Huus gfallt's D'r nimmi erächt?

        
"
"
          
Jacques

          
(setzt sich mißmutig auf die Bank):

          
Naan, ganz und gar nimmi.

        
"
"
          
Marie

          
(lauernd):

          
Un schun langer nimmi? Gell nit?

        
"
"
          
Jacques:

          
Schun arig langer nimmi.

        
"
"
          
Marie:

          
Maansch, — mir au nimmi.

        
"
"
          
Jacques

          
(überraschende):

          
Dir g'fallt's au nimmi? -Oho?-

        
"
"
          
Marie:

          
Aw'r Jacques, hesch denn noch nix gemerkt?!

        
"
"
          
Jacques:

          
Abah.

        
"
"
          
Marie

          
(erstaunt):

          
Hesch denn iewerhaupt noch gar nie ebs gemerkt?

        
"
"
          
Jacques:

          
Kaan Krimmele.

        
"
"
          
Marie

          
(schmollend):

          
Aw'r Jacques!

        
"
"
          
Ja‚cques:

          
Ja‚ dü liewi Zytt, was hawich denn merke selle? — M'r macht sini Ärwet de Daa iewer, loßt sich alle
          finf Minüte vun d'r Madamen anhüche und abkanzle, daß m'r üs d'r Hüt erüsfahre un sich trürígi d'rnäwe
          setze möecht; m'r plänkelt üf de Fyrowe — — un so gehn aam no die Dääj erum-

        
"
"
          
Marie

          
(seufzt):

          
Ja‚, so gehn aam d'rnoo die Dääej erum, un d'rbie wurd m'r no als alter un alter — jaja - — —

        
"
"
          
Jacques:

          
's isch au schun e schen Zyt here, zyter aß Dini Konfermation gemachti hesch, gell Marie?

        
"
"
          
Marie

          
(seufzend):

          
Dies isch's jo gradi.

        
"
"
          
Jacques:

          
Was denn?

        
"
"
          
Marie:

          
Aw'r Jacques, merk'sch denn als noch nix?

        
"
"
          
Jacques

          
(springt auf):

          
Zem Gücksel noch emol, was soll ich denn Allewil merke?!

        
"
"
          
Marie:

          
Jacques, Dü bisch e bissel viel aanfälti.

        
"
"
          
Jacques:

          
Aanfälti?

        
"
"
          
Marie:

          
Eijetli — nit aanfälti — — —

        
"
"
          
Jacques:

          
Aw'r e bissel viel dumma, hein?

        
"
"
          
Marie

          
(lächelnd):

          
Zell will ich jetz nit gradi saawe, aw'r-

        
"
"
          
Jacques:

          
Höer, Marie, jetz glauw’ iewer halwer daß ich ebs merk.

        
"
"
          
Marie

          
(Fräid):

          
Ja‚?! — Was denn?!

        
"
"
          
Jacques:

          
Bscht! — Numme langsam! — Dü hesch doch noch meh uffem Härze?

        
"
"
          
Marie

          
(Fräid):

          
O ja, noch arig viel!

        
"
"
          
Jacques:

          
Ei na, ze babbel numme. 
          
(lachende)
 Ich merk ebs! — O je, ich merk ebs! - Hahahahaha!

        
"
"
          
Marie:

          
Jacques -kurz un Guten — gieb m'r Antwort üf mini Fröije — — —

        
"
"
          
Jacques:

          
Uff was fer Fröije?

        
"
"
          
Marie:

          
Jacques, löij, mir sin jetz schun e paar Jährle do bynander in dem Platz. Mir han's — ei- jetli ich
          hab's so noochetnooch d'rzü gebroocht, daß mir per ""Dü"" mit-nander stehn, aw'r sunscht isch's im
          Allgemeine gradii noch's nämli wie am erschte Daa, wie m'r uns kenne han Lehr. — 's kummt m'r gradii vor,
          wie wenn m'r vor ere himmelhoche Müür stehn däete. — Do steht d'M üür — do stehn mir — un kaan Macht
          stellt sich in wie die Müür umrennt — un do stehn mir Zwei ewwe no, hilflos wie zwei klaani Kinder, un
          kumme nit wyttersch, — un kumme nit wyttersch — — —

        
"
"
          
Jacques

          
(gähnend):

          
Ei no bliewe m'r halte e Gottsname vor dere Müür stehn. — Weisch, ich-bin kaan Frinde vum klettere,
          bsunders wenn's höcher owenüs soll gehn. — Oho!

        
"
"
          
Marie:

          
Aw'r Jacques, ich hab jo numme e V'rglich gstellt.

        
"
"
          
Jacques:

          
Gott sej Danke! — Ich Schwetze nämli dene Wäj schun! — Aw'r höer, ich hab doch gemaant, Dü wottsch
          mich ebs fröije?

        
"
"
          
Marie:

          
Ja‚, — ei nadierli hawich Dich ebs fröije welle.

        
"
"
          
Jacques:

          
Na, ze fröij doch emol, ich bin g'schpanne wie e Bareplee biem Räejewetter.

        
"
"
          
Marie

          
(momentan etwas verwirrt):

          
Jacques — Dü — Dü gehsch od'r Dü wottsch iew'r kurz od'r langer üssem Dienscht?

        
"
"
          
Jacques:

          
Isch dies au wied'r e V'rglich?

        
"
"
          
Marie:

          
Ich bin am fröije.

        
"
"
          
Jacques:

          
Ah jaso. — Na, ze fröij wyttersch.

        
"
"
          
Marie:

          
Dü machsch dies Johr nimmi ganz üß in dem Platz do?

        
"
"
          
Jacques:

          
Wer sät zell?

        
"
"
          
Marie

          
(bestimmt):

          
Ich!

        
"
"
          
Jacques:

          
Hesch D'r d'Kart schläuj loon?

        
"
"
          
Marie:

          
Jacques, ich bin am fröije.

        
"
"
          
Jacques:

          
Ah ja, zem Dunterlättel — — —

        
"
"
          
Marie:

          
Un wenn Dü no do furt gehsch —

        
"
"
          
Jacques:

          
Wart e Moment, ich will mich zerscht setze — 
          
(setzt sich auf die Bank)
 So, jetz wytterscht

        
"
"
          
Marie:

          
Ja‚, un wenn Dü no do üB dem Platz gehsch, no bisch-zem letschtemol in Dim Läwe Do- mestique gsin
          ?

        
"
"
          
Jacques:

          
Wenn ich vun do furt geh — jaja, zem. letschtemol no! — No isch üsgedomestiquelt.

        
"
"
          
Marie

          
(Fräid):

          
Un mit Dim zammegsparte V'rmüije fangsch d'rnoo e Krämel an — — —

        
"
"
          
Jacques:

          
In d'r Krämergass?

        
"
"
          
Marie:

          
Un d'rnoo gründsch D'r e eijener Hüsstand?!

        
"
"
          
Jacques

          
(aufhorchend):

          
Hein?! — Was fer e Stand?!

        
"
"
          
Marie

          
(unbeirrt):

          
Löijsch Dich no nooch eme schen, brava, rechtschaffene Maidl um — —

        
"
"
          
Jacques:

          
Hein?! — Worum jetz gradi nooch eme Maidl?

        
"
"
          
Marie:

          
Dü nimmsch e Maidl wie Kocha, nähje, strieke, wäsche, Strümpf stopfe un sunscht alles mache kaan —
          — —

        
"
"
          
Jacques:

          
Marickel — hahahaha — ich merk schun wied'r ebs!

        
"
"
          
Marie:

          
Un dies Maidl hyrootsch no — — —

        
"
"
          
Ja‚cques:

          
Oeh la, Schimmel! — Ja‚ Hepplamepperle! — Ich merk's, wo's nüs will! — Ah, un Dü hesch anoch gemaant
          ich merk nix! — 
          
(Singen)
""Dü hesch jo gar kaan Ahnung, wie hel d'r Schääkele isch"".

        
"
"
          
Marie

          
(eifrig):

          
Ei na, wenn Dü no hel genuej bisch, no wäre-m-'r au enandernooch Maan un Frau un krieje enander vun
          Daa ze Daa Liawe — un d'r- 
          
noo läwe-m-'r glichli mitnand -un d'rnoo — un d'rnoo kumme die herzigi klaane,
          wuschberliche, süeße Bébé — — —

        
"
"
          
Jacques

          
(ist langsam aufgestanden):

          
Sunscht nix wie Knepfle! Naan, Mamsel Marie, sie kumme noch nit! — Aeh-äh — sie kumme noch nit! —
          Iewerhaupt — nooch mienere Meinung, laaft schun wied'r viel zeviel vun dem Gerührs do erum. Jetz sogar
          noch Negerle un Anamitle!

        
"
"
          
Marie:

          
Aw'r Jacques!

        
"
"
          
Jacques:

          
Na, ich hab jo nix do d'rgeje. — E jeds Dhierel het sin Pläsierel, un jedem Narr gfallt sini
          Kapp!-

        
"
"
          
Marie:

          
Jacques!

        
"
"
          
Jacques:

          
Was beleebt?

        
"
"
          
Marie:

          
Gell Dü hesch die klaane, herzigi Kinderle gern?

        
"
"
          
Jacques:

          
O ja. Arig gern, aw'r numme in andere Lyt ihri! 
          
(Es klingelt.)

        
"
"
          
Marie

          
(Ürgerlich):

          
D'Madamen schellt! — Isch's jetz fer mich od'r fer Dich? 
          
(eilt geje rechts.)

        
"
"
          
Jacques:

          
Sie sell warte, die alt Schatulle — 
          
(eilt Marie nach und zieht sie an der Hand wieder zurück.)
 Numme nit glich geloffe! Mir
          han se dennewäej schun viel ze viel v'rwöehnt. 
          
(Es klingelt.)

        
"
"
          
Beide:

          
(Eila geje dem üsgange rechts, fahren mit dem Rechte Zeigefinger ruckartig unde der Nase
          durch, schläuj hierauf mit der Rechte Hand auf das erhobene Knie, klatschen dann einen kräftige
          schläuj in die Hände und fiehre den Finger nochmals unde der Nase durch.)

          
ffffft! — Hutzti! — Hepplamepperle! — fffft!

        
"
"
          
Beide:

          
(Eila geje dem üsgange rechts, fahren mit dem Rechte Zeigefinger ruckartig unde der Nase
          durch, schläuj hierauf mit der Rechte Hand auf das erhobene Knie, klatschen dann einen kräftige
          schläuj in die Hände und fiehre den Finger nochmals unde der Nase durch.)

          
ffffft! — Hutzti! — Hepplamepperle! — fffft!

        
"
"
          
Jacques:

          
Wenn Die maant, daß dies eso wyttersch geht mit dere Stäeje-n-üf un abrennerej, no isch se aw'r
          diesmol schlechten orientiert. — Vor alle Dinge wurd jetz emol e Düür anne gsesse. 
          
(setzt sich auf die Bank.)
 Marie, Dü bisch doch au miede wie e — — - wie e
          Dienschtmaidel?

        
"
"
          
Marie

          
(lachende):

          
Aw'r secher! — Derf ich näwe Dich sitze?

        
"
"
          
Jacques:

          
mintwäje au üf de Geere.

        
"
"
          
Marie

          
(verschämt):

          
Jo allewäej!

        
"
"
          
Jacques:

          
Ei na — d'r Madamen ze Leid — kumme! — —

        
"
"
          
Marie

          
(wie oben):

          
Naan, Jacques, es isch nit anständi.

        
"
"
          
Jacques

          
(gekränkt):

          
Ze wär ich demnooch unanständig.

        
"
"
          
Marie:

          
Naan, Jacques, ums Himmelswille, dies hawich nit saawe welle 
          
(setzt sich ihm rasch auf den Schoß.)
 Wenn aw'r d'Madamen kummt?

        
"
"
          
Jacques:

          
Sie kummt aw'r nit.

        
"
"
          
Marie:

          
Od'r wenn se uns eso sieht?

        
"
"
          
Jacques:

          
Sie sell sich um ihri Angeläjeheite interessiere.

        
"
"
          
Marie

          
(schmeichlerisch):

          
Bisch doch e seelegueter Burscht, Jacques.

        
"
"
          
Ja‚cques

          
(schmunzelnd):

          
Ja‚?

        
"
"
          
Marie

          
(streicht ihm die Wangen):

          
Bisch e Liawe Guten Kerl.

        
"
"
          
Jacques

          
(wie oben):

          
Allewäej?

        
"
"
          
Marie:

          
Wenn ich d'r saa! 
          
(Verlobten)
 Jaeques-

        
"
"
          
Jacques:

          
Was hesch uffem Härze?

        
"
"
          
Marie:

          
Ich — ich hab Dich gern!

        
"
"
          
Jacques:

          
Aha, ich merk schun wied'r ebs!

        
"
"
          
Marie:

          
Jacques, mir derfe nimmi üssenander, kummt's wies will! Mir Zwei muehn bynander bliewe, denn mir
          sin enander schun so gewohnt, daß mir aans ohni 's ander iewerhaupt nimmi lab könnte. Mit aam Woorte:
          mir Zwei muehn enander hyroothe, ich mueß Dini Frau wäre — — —

        
"
"
          
Jacques

          
(überraschende):

          
Was muesch Dü?

        
"
"
          
Marie:

          
Dini Frau wäre!

        
"
"
          
Jacques:

          
Oho! Zeij Marie, steh e bissel üf!

        
"
"
          
Marie

          
(leidenschaftlich):

          
Naan, ewwe hesch v'rlangt, daß ich D'r üf de Geere sitz, jetz blie ich aw'r au sitze! Die günschti
          Geläjeheit loß ich diesmol nimmi fahre!

        
"
"
          
Jacques:

          
O ich Dolle!

        
"
"
          
Marie

          
(wie oben):

          
Jacques, wenn Dü emol do üs dem Platz eweck gehsch, un daß mir üssenander grissa däete wäre, d'rnoo
          muesch hyroothe — iewerleij D'r's wie De witt — Dü muesch d'rnoo ewwe hyroothe! — Do springsch nit drum
          erum! Un wenn Dü d'rnoo hyrootsch, d'rnoo muesch vor alle Dinge e Frau nämme wie Dich kennt, wie Dich
          lieb het, wie Dine Charakter v'rsteht, un wie weiß, wie se mit Dir umzegehn het un wie Dü bhandelt
          muesch wäre — — —

        
"
"
          
Jacques:

          
Wo steht denn dies gschriewe, daß ich iewerhaupt emol hyroth?

        
"
"
          
Marie:

          
eenfach — Dü muesch hyroothe!

        
"
"
          
Jacques

          
(erstaunt):

          
Ich mueß?

        
"
"
          
Marie:

          
Ja‚, Dü muesch.

        
"
"
          
Jacques:

          
Wer sät dies?!

        
"
"
          
Marie:

          
Ich saa's!

        
"
"
          
Jacques:

          
Zeij, Marie, steh üf!

        
"
"
          
Marie:

          
Noch aan Moment.

        
"
"
          
Jacques

          
(verzwiefelt):

          
D'Füeß schloofe m'r in — un — un ich kriej de Datteri in d'Knej — un ich Schwetze m'r e
          pflutschnasser buckel! — V'rdoria noch emol!

        
"
"
          
Marie:

          
Dies lejt mir jetz Alles nix an! — Ich saa Dir's eenfach noch hundertmol: Dü muesch hyroothe! Denn
          wenn nit hyrootsch, no hesch jo kaan Menscheseel üf dere Welt wie üf Dich löijt un wie Dich bsorjt.
          No kriejsch kann Wäsch gewäsche, kaan Kleider gfliekt, kaan Strümpf gstopft, kaan Knopf angenähjt, un
          kaan Guten kräftige Süppele kocht — — —

        
"
"
          
Jacques

          
(kleinlaut):

          
Ei, ei, ei, ei, ei, ei!

        
"
"
          
Marie

          
(lächelnd):

          
Gell, an so Sache hesch noch gar nie gedenkt?

        
"
"
          
Jacques

          
(wie oben):

          
Naan.

        
"
"
          
Marie

          
(zärtlich):

          
Ja‚ siehsch, min Liawe Bue, m'r mueß ewwe an Alles denke! 
          
(holt einen Apfel aus der Schürzentasche, und beißt Kraft hinein.)

        
"
"
          
Jacques:

          
Was hesch denn do?

        
"
"
          
Marie

          
(kauend):

          
E-n-Apfel!

        
"
"
          
Jacques:

          
Wo hesch denn dene here?

        
"
"
          
Marie

          
(schmatzend):

          
Uessem Garte. 
          
(lachende)
 wit au e Biß?

        
"
"
          
Jacques

          
(kopfschüttelnd):

          
Aeh-äh! — Ich merk schun wied'r ebs!

        
"
"
          
Marie

          
(kauend):

          
Nimm doch e Biß! Geh! Schenier Dich doch nit!

        
"
"
          
Jacques

          
(unruehich):

          
Steh üf jetz!

        
"
"
          
Marie:

          
Zerscht dhuesch emol herzhaft drin bissi.

        
"
"
          
Jacques:

          
Na, daß Ruehj hesch — here mit! — Aw'r no stehsch üf! — 
          
(beißt Kraft von dem Apfel ab.)

        
"
"
          
Marie:

          
Isch'r Guten?

        
"
"
          
Jacques

          
(schmatzend)

          
Aw'r secher.

        
"
"
          
Marie:

          
Isch'r au süeß?

        
"
"
          
Jacques:

          
Aw'r secher!

        
"
"
          
Marie:

          
Witt noch e Biß?

        
"
"
          
Jacques:

          
Ich hab jo 's Mül noch vol.

        
"
"
          
Marie:

          
Nimm noch e klaans Bissele, geh!

        
"
"
          
Jacques:

          
Höer, Marie, so het's d'Eva zellemols im Paradiesaue mit 'm Adam getriewe.

        
"
"
          
Marie

          
(lachende):

          
Do wärdsch Dü d'r Adam?

        
"
"
          
Jacques

          
(Lachede):Un

          
Dü d'Eva! — Jetz fahlt numme noch d'Schlange. 
          
(Beide Lache Kraft.) (Es klingelt!)

        
"
"
          
Marie

          
(lachende):

          
Là voilà — doch isch se jo schun!

        
"
"
          
Jacques:

          
wahrhafti! Na, jetz wär jo so ziemli Alles bynander — numme 's Paradiesaueaue fahlt noch! Im Paradiesaueaue sin
          m'r bis hyt noch nit gsin. Gell nit, Evele?

        
"
"
          
Marie:

          
Naan jo nit. — Ehnder mittle in d'r Höll! — Aw'r mir welle jetz emol ins Paradiesaue mit-nander, Arm in
          Arm, un unser Läwe genieße un glückliche un zefriede mitnand un näwenander lab! 
          
(springt auf und zieht Jacques mit großer Kraftanstrengung in die Höhe.)
 Ich lab fer
          Dich, un Dü labsch ger mich! Gell Jacques?!

        
"
"
          
Jacques

          
(sich schwerfällig erhebend):

          
Mini Füeß sin ganz stiff worre. Un am Rechte Fueß schlooft m'r d'r groß Zeh! - Sapperlot, wie dies
          kriwwelt! — Do bisch Dü d'Schulde dran! —

        
"
"
          
Marie:

          
mintwäje. — Ich nimm jo gern alli Schulde üf mich. Un ich will d'r's jo au gern v'r- spreche, dab
          ich miener Läwe eso ebs nimmi dhun will.

        
"
"
          
Jacques:

          
hein? — Was hesch gsaat?

        
"
"
          
Marie:

          
secher! — Miner Läwe ninimi.

        
"
"
          
Ja‚cques:

          
Ja‚, so siehsch üs!

        
"
"
          
Marie:

          
Bisch nit böes gell nit? 
          
(küßt ihn.)

        
"
"
          
Jacques

          
(verblüfft):

          
Aw'r Marie?! — Eva?! —

        
"
"
          
Marie

          
(zärtlich):

          
Nit böes sin, Adam! 
          
(küst ihn) Jacques schmatzend)
: Mhmmm! — Aaaah! — Dies sell nit kriwwle! — 
          
(tritt mit dem Rechte Fuß höcher in die Höhe.)
 Jetz schlooft m'r d'r groß Zeh gar nimmi! —
          
          
(reekt sich rasch in die Höhe und breitet die Arme aus.)
 Marie, Evele, noch e Verschmutze! —
          Noch aaner! 
          
(Marie fliegt ihm an die Brust und küßt ihn.)

        
"
"
          
Jacques

          
(schmatzend):

          
O V'rdoria noch emol! — Noch aaner!

        
"
"
          
Marie

          
(windet sich lachende los):

          
— Naan, min Liawe Adam, jetz isch's genüej! 
          
Es klingelt!)

        
"
"
          
Marie

          
(lachende):

          
Siehsch — jetz het's gschellt! — Do kannsch nix mehr dran mache! Jacques 
          
(stampft zornig mit dem Fuß auf):
 Zell welle m'r aw'r doch emol sehne! 
          
(Es klingelt!)

        
"
"
          
Jacques:

          
Jaja, m'r kumme, alti Schawäll! Diesmol kumme m'r alli Zwei mit-nander! — Warte Sie numme, Madamen,
          diesmol selle Sie sich wundere, wenn ich anfang ""höefli"" zu wäre! - Uffgekündt wurd! — Uff d'r Stell! —
          Marie, machsch mit?!

        
"
"
          
Marie

          
(triumphierend):

          
Aw'r secher mach ich mit!

        
"
"
          
Jacques:

          
batsche in! — 
          
(halte ihr die Rechte Hand entgegen.)

        
"
"
          
Marie

          
(Feierlicher):

          
E Mænner — e Woorte! 
          
(schlägt kräftigt ein.)

        
"
"
          
Jacques:

          
Un noch emol e Verschmutze!

        
"
"
          
Marie

          
(lachende):

          
Naan — jetz nit — später — wenn m'r emol ghyroot sin. 
          

          
(Es klingelt andauernd heftig.)

        
"
"
          
Jacques:

          
Höersch — Marie — sie lytte schun —

        
"
"
          
Marie

          
(schelmisch lächelnd):

          
Zue unsere Huchzytt!

        
"
"
          
Jacques

          
(wirbelt Marie in ausgelassen Stimmung im Kreisel herum.)

          
Juuuuuuuuh! — kumme, Marie, m'r packe unser Bimbelebaasch zamme, un d'rnoo aw'r nix aß wie nüs üs
          dere Höll! :

        
"
"
          
Marie

          
(jubelnd):

          
Un nin ins Paradiesaue!

          
(Beide hüpfen, eng umschlungen, jubelnd rechts ab.)

        
"
