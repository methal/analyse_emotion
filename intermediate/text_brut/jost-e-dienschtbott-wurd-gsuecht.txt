"
          
MEYER:

          
(hett e Zyttung in d'r Hand unn geht aufgeregter üf  unn ab)

          
Nein, diss iwerläw ich nitt ! Myn ganz Vermeje haw ich verloren! E ganzi Million isch füddi, wo ich in Aktien vun d'r « Australisch Auschterebank » g’steckt habb, unn jetzt isch diss ganz unternumme iwer Nacht verkracht ! Do steht’s im Blättel !

        
"
"
          
MEYER:

          
Unn do bisch numme dü dran Schuldige, Jean !

        
"
"
          
JEAN :

          
Na ja, ,patron..., ich will jo gern zuegänn, ass ich denne Morjen Ihri Café-Schissel verhejt habb, awer ...

        
"
"
          
MEYER:

          
Ach was, die meinenen ich nitt! Vun mir üss hättsch d'ganz G’schirrabteilung vum « Magmod » kapütt dengle kenne! Ich meinenen d’« Australisch Auschterebank»!

        
"
"
          
JEAN:

          
Ah! Isch s’e üss’m Lym gange? Unn ich soll Schuldige sinn ? Ich habb mich nitt druff g’setzt, Monsieur Meyer !

        
"
"
          
MEYER:

          
Babbel kein Mischt, Jean! Ass s'e schiewes isch gange, isch nitt dyni Schulde, awer ass ich myn ganz Gald nyng’steckt habb !

        
"
"
          
JEAN :

          
Jetzt wurd’s Daa! Ja‚, ,patron..., saawe emool: Sinn Sie d'r Herr odder ich? Kenne Sie nitt mache, was Sie welle ?

        
"
"
          
MEYER :

          
Doch, awer wo ich dich angaschiert habb, hesch dü mir versichert, ass dü dich üf alles verstehsch.

        
"
"
          
JEAN:

          
Sell isch wohrer, wenn sich's um ’s Kocha, buetze, Nähje unn so wyddersch handelt : ass ich awer au noch Ihre Finanzrat soll sinn, diss isch nitt im Kontrakt vor- g’sähn !

          
        
		
"
"
          
MEYER :

          
Egal, dü hesch mir awer geroote, ass ich’s riskeere kennt, myn Vermeje in dem unternumme anzeleije.

        
"
"
          
JEAN :

          
Unn ich dhät’s noch emool mache, Monsieur Meyer ! Ich bin in dem Dings Guten renseigniert ..... myner bescht Frinde hett jo e Liebschter, wo d’Schweschter vun d'r Butzfrau vum Gouverneur vun d'r Banque de France isch ! Non, non, ich loss mir ’s nitt üssredde ..... die Ba- brüehle sinn so secher wie Zuchthüssmüre !

        
"
"
          
MEYER :

          
Ich habb jo ’s Konträr devun schwarzer üf wyss ! Füddi isch füddi! Unn fur ass ich an die unglieckselig Affär gar nimm ze denke brüch, ze will ich mir die Aktie-Babierer do üss de Aue schaffe ! 
(Er langt e En- veloppe üss'm Sack, wo e paar Babierer drine sinn unn leijt s’e üf de Disch)
: Wo sinn denn Schwäwehelzle ..... ich will s’e verbrenne! 
(Er suecht im Zimmer nooch 's Schwäwehelzle)
. JEAN 
(nemmt g’schwind, unn ohne ass ’s d'r Meyer sieht, d'Babierer üss d'r Enveloppe, steckt s’e yn unn ersetzt s'e durich e Stick Zyttung, wo üf'm Disch lejt ; fur sich)
: Ich Glöüwe als noch nitt an denne Bankkrach unn waje dem will ich die Babierle do in Sicherheit bringe ..... nutzt’s nix, ze Schade’s nix !

        
"
"
          
MEYER:

          
Ah, do sinn jo Schwäwes! 
(Er zind eins an unn Verbrennt d’Enveloppe)
 : Diss isch, ma foi, e dyer’s Fyer- werik, wo mich e bari Million koschte dhuet! Ich Ar- mer !

        
"
"
          
JEAN :

          
Wenn diss numme kein Dummheiten isch, ,patron...!

        
"
"
          
MEYER:

          
Was, Dummheiten ? Was erlaubsch dü dir iwerhaupt, mich dumma zu häise! Dü kummseh mir iwerhaupt in letschter Zytt allewyll so fräch !

        
"
"
          
JEAN :

          
Uff jede Fal, ich hätt’s nitt gemachti.

        
"
"
          
MEYER:

          
Ich will eenfach nimmi an denne Fehlschlaa ge- mahnt wäre, unn waje dem kannsch au dü sofort dyne Ranze packe unn dich um e neje Platz umsähn, denn wenn ich nitt üf dich g’horicht hätt, ze wär ich hytt nitt am Bettelstab !

        
"
"
          
JEAN :

          
Was ? Unn waje dem welle Sie mich schasse ?

          
		  
        
"
"
          
MEYER :

          
Oh, nitt numme waje dem ! Dü bisch mir in letsch- ter Zytt au viel ze viel iwer de Kopf gewachse! Dü hesch gemachti, was de witt, unn ich habb d’Roll vum e Dienschtbott kenne spielen.

        
"
"
          
JEAN :

          
Wie wenn ich’s nitt allewyll Guten mit Ihnne gemeint hätt ! E Fröiij, ob Sie e bessere Noochfoljer fur mich be- kumme ? Sie sinn numme noch nitt im Rechte in d’Fin- ger g’falle !

        
"
"
          
MEYER:

          
Egal! Dü kannsch gehn ... ich habb schun e An- nonce in d’Zyttung gemachti unn warte alle Moment üf e nejer Dienschtbott.

        
"
"
          
JEAN :

          
Na, ze will ich nitt noch langer by Ihnne anhalte, unn ich wünsche, ass ’s Ihnne nie lid dhuet, mir de Laufbass gen ze hann !

        
"
"
          
MEYER :

          
Mir lid dhuen! Im Läwe nie! Unn jetzt ver- schwind ! 
(Kehrt'm de buckel anne.)

        
"
"
          
JEAN :

          
(fur sich)

          
Halt, ich habb e Guten Idee, denn ich habb myne Herr viel ze lieb, als dass ich’ne so eenfach uffgibb ! 
(Rächts ab)
.

        
"
"
          
MEYER :

          
Gott sej Danke, ass’r furt isch ! Diss wär doch gelacht, wenn’s kein bessere Ersatz fur’ne sott gen ! Denne will ich awer no dressiere ! Du reste, haw ich mir do e Photographie-Apperat angeschafft, fur myn däjlich’s Brot mit ze verdeene, denn nooch dem Millioneverluscht kann ich mir’s nimm läischte, mich üf d’füll Hütt ze leije. 
(Er stellt de Apperat by d'r Dier rächts üf unn macht sich dran ze schaffe.)
 Awer ich muess erscht noch e bis- sel Lehr unn stüdiere, wie die Photographiererej funk- tioniert, unn wenn ich’s no kann, ze geh ich nüss üf’s Landen unn photographier d’Büre unn ihre Vieh unn ihri Fraue.... do isch noch Gald mit ze verdeene ! 
(’s klopft.)
 Wer kummt denn jetzt ? Entrez!

          

        
"
"
          
EMIL :

          
(e groteski Figür)

          
Wohnt do d'r Monsieur Meyer ?

        
"
"
          
MEYER:

          
Ja‚; der bin ich sälwer !

        
"
"
          
EMIL :

          
(fur sich )

          
Na, dem will ich’s stecke! Myner Frinde, d’r Jean, hett mich g’schickt ! 
(Lütt)
: Ich kumme waje dere Annonce, wo in d'r Zyttung isch unn .....

        
"
"
          
MEYER:

          
Ah, Guten! Awer ein Moment... ich habb gradi myne Photographie-Apperat yng’stellt unn will gschwind e Prowe-Uffnahm mache, wyll d’Sunna im Mo- ment so günschti isch. stella Ejch do anne ! 
(Er stellt de Emil e paar Schritt vor’m Apperat üf.)

        
"
"
          
EMIL:

          
Hoffetlich gitt’s kein uffnemme fur ins Verbrecher- Album !

        
"
"
          
MEYER :

          
Unn jetzt lueje e bissel friendlicher dryn !

        
"
"
          
EMIL :

          
Ich kann mit’m beschte Wille nitt.

        
"
"
          
MEYER:

          
Allewäj nitt! Denke an ebbs Guets !

        
"
"
          
EMIL :

          
Ich habb schun langer nix Guten’s meh g’hett mit Üss- nahm vum e saftige Styerzettel !

        
"
"
          
MEYER:

          
Also ze denke emool an e zart’s, saftige’s Kalbs- schnitzel ! EMIL 
(laachter e bissel)
.

        
"
"
          
MEYER :

          
..... so gross, ass’s üf alle Sytte vum Deller erab- hängt... EMIL 
(Lächelt starick)
.

        
"
"
          
MEYER :

          
..... unn e Platt vol pommes frites mit Salat ..... EMIL 
(grinst unn’s Wasser lauft'm üss’m Mül üf de Bodde ; er butzt sich mit’m Nasduech ab.)

        
"
"
          
MEYER:

          
..... unn e Liter Gähler dezue .....

        
"
"
          
EMIL :

          
(strahlt)

          
Herr, here üf, odder ich lauf üss wie e vol Eimer, wo e Loch hett! Geje Sie isch jo d’Folter im Middelalter ’s reinscht G’sellschaftsspiel !

        
"
"
          
MEYER:

          
So isch’s Guten ..... blyewe so... ich will g’schwind scharf ynstelle ! 
(Er stellt sich hinter de Apperat, nemmt’s schwarzer Duech iwer de Kopf unn de Apperat unn gückt durich.)
 So! Ja‚, ’s geht! 
(Allewyll noch unde'm Duech)
: Unn jetzt gücke in de Apperat nyn !

		  

		
"
"
          
EMIL :

          
(geht an de Apperat unn linzt mit eim Au nyn)

          
Ich sieh nix !

        
"
"
          
MEYER :

          
(werft vergelschtert ’s Duech furt)

          
Herrjeh, was isch denn diss fur e Riese-Ochse-Au in mym Apperat ? 
(Sieht de Emil vorm Apperat.)
 Ah, denne Wäj! Na, Ihr schyne mir d’Dummheiten au mit’m Leffel gesse ze hann ! Also lonn m'r ’s sinn mit dere uffnemme ..... ich habb Angst, d’Platt kennt m'r verspringe, wenn ich’re zuemuete dhät, Ejeri iwerzwerich Visasch ze verewige ! Also zuer Sach..... Ihr hann also myni Annonce ge- läse ? 
(Emil nickt.)
 Wie stehts: Kenne Ihr Kocha ?

        
"
"
          
EMIL:

          
Nein!

        
"
"
          
MEYER :

          
buetze?

        
"
"
          
EMIL:

          
Nein!

        
"
"
          
MEYER :

          
Nähje ?

        
"
"
          
EMIL :

          
Nein!

        
"
"
          
MEYER:

          
Flicke?

        
"
"
          
EMIL:

          
Nein!

        
"
"
          
MEYER:

          
Ja‚, was welle Ihr denn no eijetlich do ?

        
"
"
          
EMIL:

          
Ei, ich habb Ihnne numme welle saawe, ass ich die Stell nitt annahme kann !

        
"
"
          
MEYER:

          
Jetzt hett’s awer g’schellt ! Nein, so e Frechheit ! Nüss !, saaw ich, nix als wie nüss! Ihr hann mir gradi noch g’fählt !

        
"
"
          
EMIL :

          
(laachter)

          
So? No kenne Sie mich jo au angaschiere !

        
"
"
          
MEYER :

          
Ehnder zeh Jean als wie Ejch!

        
"
"
          
EMIL :

          
(fur sich)

          
Diss haw ich wisse welle ! Ich will’s im Jean glych saawe ! 
(Links nooch hinte ab.)

        
"
"
          
MEYER:

          
Nix als wie verzirne kamm’r sich! Oh, ’s isch e Elander mit denne Dienschtbotte vun hyttzedaas ..... fräch sinn s’e wie e Spatz unn s’e welle Liawe meh Lohn 
 unn defur e bissel wenjer Ärwet! Na, ich will Liawe noch e bissel photographiere, denn ich muess’s partout Lehr ; ’s isch, du reste, au e ganz g’fitzter Sport... schen wie jeder ander. So haw ich mich, par exemple, au schun mit d'r Fischerej abgänn ..... d’an- dere hann d’Fisch g’fange un ich habb alti Schlappe, Hafe unn ein Protokoll um’s ander odder gar de Sunne- stich verwitscht! No bin ich üf d’Jagd gange, unn was habb ich gedroffe ? Myne Hund odder gar de garde- chasse !... Oh, ich därf gar nitt an alles denke ! Hoffet- lich klappt’s mit'm Photographiere besser! Eins macht mir deby allerdings jetzt schun Küemmer, unn diss isch, ass alli myni Bilder so arich verschwumme sinn... unn diss kummt dohere, wyll sich alli Lytt, wo ich bis jetzt photographiert habb, bewejt hann. Waje dem mach ich’s jetzt fur de anfang andersch.... ich photographier eenfach die Statüt do, wo sich wohrschynnlich nitt so ball beweje wurd ! 
(Er stellt d’Statüt mit’m G’stell links vun d'r Scène e paar Schritt vorm Apperat üf.)
 So, unn jetzt welle mir ynstelle ! 
(Er schlupft unde’s Duech unn schafft am Apperat erum )
 er kann deby allerhand Kapriole mache, so ass d’r Apperat wackelt unn als schier umfalle will.

        
"
"
          
JEAN :

          
(ohne ass d'r Meyer, wo am Apperat erumschafft, druff acht gitt)

          
So, do bin ich widder! Jetzt will d'r Jean emool sym Liawe Monsieur Meyer zeije, was fur e Unterschied zwische mir unn andere Dienschtbotte kann sinn ! Waje dem haw ich mich g’schwind verkleid, fur ass’r mich nitt kennt. Zuedem weiss ich jo gar ze Guten, ass’r nitt ohne mich exischtiere kann ! Ah, do isch’r jo! Wie’r numme mit dem Apperat umgeht... wie wenn’r’ne knock out boxe wott !

          

		
"
"
          
MEYER :

          
(als noch unde'm Duech)

          
So, d’Statüt wär exakt yng’stellt! Jetzt kummt d’Platt nyn. 
(Nemmt üss’m Fracksack e Kassett, wo’r in de Apperat schiebt. Er kummt unde'm Duech vor unn bedrachte numme noch de Apperat, so ass’r de Jean, wo in d'r Nähde vun d'r Statüt steht, nitt sieht.)

        
"
"
          
JEAN :

          
(stellt sich g’schwind vor d’Statüt ; fur sich)

          
Ich bin doch schun langer nimm abkonterfeit worre !

        
"
"
          
MEYER :

          
(wo nix devun sieht, nemmt de Knipser vum Apperat in d'Hand)

          
Unn jetzt nimm geriehrt ..... rachte fryndlieh, s’il vous plait!... Eins-zwei-drej ! 
(Er zählt mit de Finger.)
 So! Diss wär g'schahn ! 
(Nemmt d’Kassett üss’m Apperat.)
 Jetzt will ich die Platt g’schwind ent- wickle gehn ! 
(Ab nooch rachtes mit d'r Kassett.)

        
"
"
          
JEAN :

          
Diss isch jo allerhand... d’r Monsieur Meyer isch eso verdieft in syni Knipserej, ass’r mich noch nitt emool g’sähn hett !

        
"
"
          
MEYER :

          
(in d'r Hand e Glasplatt)

          
Was isch mir denn nitt diss ! Myner Napoléon hett sich jo in e Bür verwan- delt... unn e Däschelskapp hett’r au noch üf! 
(Er sieht jetzt de Jean)
: Ah, do steht der Sindebock ! Was han Ihr denn in mynere Menasch unn gar noch in mym Photographie-Apperat ze suche ?

        
"
"
          
JEAN :

          
Ich habb in d'r Zyttung Ihri Annonce geläse unn bin kumme waje dem Platz, wo by Ihnne freia isch.

        
"
"
          
MEYER:

          
Ah, denne Wäj! Also, was kenne Ihr ?

        
"
"
          
JEAN :

          
Alles !

        
"
"
          
MEYER:

          
Diss isch nitt gradi wenni, wenn’s wohrer isch ! Unn was wär diss par exemple ?

          

		
"
"
          
JEAN :

          
(fur sich)

          
Na, warte, ich will dirs schun Kocha! 
(Lütt)
: Ich kann e Ochs anspanne... also wurr ich Ihnne wohler au d’Kleider anzehje kenne !

        
"
"
          
MEYER:

          
Ihr schyne um Verglych nitt gradi arich verlegener ze sinn!

        
"
"
          
JEAN :

          
No kann ich Gras mähje ..... also wurr ich wohler au Ihre Bart rasiere kenne !

        
"
"
          
MEYER:

          
Unn wie steht’s mit d'r Kiche ? JEAN ch Oh, ieh bin e grosser Frinde vun’re Guten Kiche! Unn no au vum e Guten Gläsel Wyn... ich syberel fur myn Läwe gern !

        
"
"
          
MEYER:

          
Ah, Ihr syberle gern! 
(Fur sich)
: Er isch noch Jungi unn unerfahre, awer mit d'r Zytt kann’r sich so mache, wie ich’ne hann will! 
(Lütt)
: Unn was hann Ihr fur e Charakter ?

        
"
"
          
JEAN:

          
Oh, ich habb de bescht Charakter vun d'r Welt! Ich mach alles, was m'r will... unn sogar diss, was m'r nitt will! Kurz, e Lämmele isch geje mich e Wolf !

        
"
"
          
MEYER:

          
Unn wie häise Ihr ?

        
"
"
          
JEAN:

          
Ich häisser ganz kurz: Charles Auguste Leon Pierre Georges Louis Alfred Jean Dippeldappel !

        
"
"
          
MEYER:

          
Unn diss häise Ihr kurz! So e Schlange vum e Namme kann jo kein Mensch b’halte !

        
"
"
          
JEAN :

          
So, kein Mensch ? Ich kann’e jo au b’halte.

        
"
"
          
MEYER:

          
Diss wär noch schen, wenn Ihr Ejere eije Namme nit b’halte kennte ! Bref, ich ruef Ejch noch eenfach : Alfred !

        
"
"
          
JEAN :

          
Nein, Jean!

        
"
"
          
MEYER:

          
Ich zej Alfred vor! 
(Fur sich)
: De Jean will ich nimmi heere !

        
"
"
          
JEAN :

          
Ich blyeh by Jean ! Unn wenn Sie mich noch langer säje, ze dhue ich Ihnne d’Fenschterläde blöuj poliere ! 
(Er wurd als fräch.)

        
"
"
          
MEYER :

          
(baff)

          
Unn der will e Lämmeles-Charakter hann ! Nein, myn Liawe, in dem Fal verzieht ich üf Ejeri Dienscht ..... do isch mir d'r Jean, diss heisst d’r Wolf, doch noch Liawe als wie d'r Wolf im Schoofsbelz ! Ich nemm sogar noch Liawe e Maidl fur alles !

          

        
"
"
          
JEAN :

          
Wo sich awer nitt alles g’falle losst wie par exemple so e stilli, verträjlichi Natür als ich eini habb! Wisse Sie, was Sie sinn ? E Gyzhals sinn Sie! E Gro- bian ! E Lyttschinder ! E Aeltererr, yngebilder Zipfel ! 
(D’r Meyer hüpst by jedem Woorte vergelschtert in d’Heeche.)
 So Lytt wie Sie hett m'r friehjer üf d’Läbkueche ge- bäbbt! Sie üfgewärmter Leichnam ! Ich habb d’Nas vol vun Ihnne... ich geh... wäre selige ohne mich! 
(Er nemmt syni Kapp unn wärft s'e im Meyer ins G'sicht ; fur sich)
: Diss wurd hoffetlich langer, fur ass’r ynsieht, was’r an sym Aelterer, Guten Jean verloren hett! 
(Ab nooch hinte.)

        
"
"
          
MEYER:

          
Hett m'r jetzt schun emool so e boddelosi Unver- schämtheit erläbt! Er hett wajer Zytt g’hett, ass’r sich üss’'m Staub gemachtii hett, der grobi Flejel, denn schunscht hätt ich'm au emool myne Charakter gezeijt ! So wytt sinn mir also hytt kumme, ass d'r Herr Angst muess hann vor de Dienschtsocke ! So leije sich also die füelle Harrschaft de begryffe « Egalité» üss... die meinen, sie därfe gradii eso fräch unn grobi sinn als wie unsereins! Do isch d'r Jean doch noch e anderer Burscht g’sinn ! Er hett zwar alles nooch sym Schäddel gemachtii, awer er hett mich doch als schliesslig ze iwer- zeje gewisst, ass eijetlich ich’s bin g’sinn, wo’s vun vorneryn eso gewellt hett ! Hätt ich’ne numme nitt furt- g’schickt... gradii jetzt, wo ich im Elander bin, hätt ich e starick Halt an’m g’hett! Wenn ich numme wisst, wo’r sich uffhalte dhuet... nein, ’s gängt jo doch nitt, ass ich mich als Herr eso diefi erabbloss, fur ihm nooch- zelaufe unn’m Guten Wertle ze gen ! Schade... arich Schade! g'schahn isch g'schahn, was hyllsch ! Ze will ich Liawe noch e bissel photographiere unn g’schwind e anderi Platt hole ! 
(Rächts ab.)

          
        
		
"
"
          
JEAN :

          
So, jetzt will ich emool sähn, ob d'r Monsieur Meyer sich e Lehre genumme hett ! Ich habb’m jo vorig d'hel häisser genue gemachti !

        
"
"
          
MEYER :

          
(macht sich am Apperat ze schaffe)

          
Unn jetzt die nej Platt in de Apperat !

        
"
"
          
JEAN :

          
(stellt sich widder vor d’Statüt ; fur sich)

          
Wie der spanne wurd, wenn’r d’Platt entwickelt !

        
"
"
          
MEYER:

          
Kein Bewejung meh! Rechte friendlicher, s’il vous plaît ! Eins-zwei-drej ! 
(Er nemmt d’Kassett üss’m Apperat.)
 Hoffetlich isch’s diss Mool besser gange! 
(Rechtes ab.)

        
"
"
          
JEAN :

          
Er hett mich als noch nitt g’sähn ! Wenn’r wisse dhät, was fur e Guten Nuwell ass ich fur’ne habb, er dhät sich mit dem Gückkaschte gewiss nimm abschinde !

        
"
"
          
MEYER :

          
Jetzt heert sich doch alles üf ! Myner Napoléon hett sich diss Zugg gar in de Jean verwandelt! Wie isch diss mejlich ? 
(Er sieht de Jean. )
 Ah, do steht’r jo! 
(Fur sich)
: Er kummt mir wie geruefe ! Awer ich muess’ne e bissel zawle lonn ! 
(Lütt, Strenge)
: Was welle Sie do, Monsieur ? Wisse Sie nitt, ass myni Hüssdier fur Sie fermé isch ?

          

		
"
"
          
JEAN:

          
Ich habb s’e awer offenerner g’funde, Monsieur Meyer ! Unn wenn e Dier offenerner isch, ze isch diss e Zeiche, ass m'r nyn kann unn ...

        
"
"
          
MEYER:

          
..... unn ass m'r au widder nüssflieje kann !

        
"
"
          
JEAN :

          
Ich bin au gar nitt kumme, fur Ejer schen’s G'sicht ze sähn ..... ich will numme d’Kapp hole, wo myn Ka- merad vorig do hett leje lonn... ich soll se’m bringe! 
(Er setzt d’Kapp üf, wo noch üf'm Bodde lejt.)

        
"
"
          
MEYER :

          
(baff)

          
Im Himmel !

        
"
"
          
JEAN :

          
Nein, nitt in de Himmel, awer in d'Wirtschaft vis-à- vis... er warte dort üf mich !: à

        
"
"
          
MEYER:

          
Was sieh ich ? Die Kapp ! Diss G'sicht ! Diss isch jo ’s nämlich G'sicht wie diss vum Alfred, vun dem un- g’schliffene Holwel, wo sich vorig by mir vorg’stellt hett! Wo haw ich denn numme myni Aue g’hett !

        
"
"
          
JEAN:

          
Kein Wunder, ass ich'm glych ! Ich bin üss’m nämliche Ort wie er !

        
"
"
          
MEYER:

          
Am End noch verwandt mit’m ?

        
"
"
          
JEAN:

          
Er hett de nämlich Vadder wie ich.

        
"
"
          
MEYER:

          
Ah so!

        
"
"
          
JEAN:

          
Unn ich bin’s einzige Kind vun mym Bäbbe !

        
"
"
          
MEYER :

          
Haw ich dich verwitscht, dü üssgekochter Spitz- bue ..... dü farceur ! Jetzt sieh ich hel: Dü hesch denne ung’howelter unn gewalttätig Alfred g’spielt, fur ass mir d’Luscht üf e anderer valet-de-chambre vergehn soll ?

        
"
"
          
JEAN :

          
Ich kann’s nitt leigle, Herr !

        
"
"
          
MEYER:

          
Ja‚, Jean, haltsch denn dü so arich an mir ?

        
"
"
          
JEAN :

          
Unn wie, Herr!

        
"
"
          
MEYER:

          
Unn au in mym Elander witt de mich nitt im Stich lonn ?

        
"
"
          
JEAN :

          
Was, Elander ? Herr, mir sinn jo rych ! Do steht’s in d'r Zyttung! 
(Er gitt im Meyer d’Zyttung, wo üf'm Disch lejt.)
 D’« Australisch Auschterebank » isch jo gar nitt verkracht !

        
"
"
          
MEYER :

          
Was saasch de do, Jean ! 
(Läst)
: « Unsere gestrige Mitteilung über den Zusammenbruch der « Australischen Austernbank » beruht auf einer Falschmeldung. Im 
 Gejeteil, ihre Aktienn sind sogar seit gestern um den doppelten Wert gestiegen.» 
(Er fallt üf e Stuehl.)
 Oh, ich g’schlaawener Mænner ! Jetzt haw ich myn Vermeje erscht erächt verloren ! Diss verwind ich nitt! Ich habb jo myni Aktien Verbrennt !

		
"
"
          
JEAN :

          
(laachter)

          
Diss wär allerdings jetzt d'r Fal, wenn ich so e dumma Diener wär, wie Sie vun rächtswäje eine verdeene dhäte! 
(Er langt d’Aktien üss’m Sack unn gitt s’e im Meyer.)
 Ich habb s’e nämlich noch rächtzyttig in Sicherheit gebroocht ! Was Sie Verbrennt hann, sinn alti Zyttunge g’sinn !

        
"
"
          
MEYER:

          
Isch’s mejlich, Jean ! Dü bisch e Prachtsmensch, e Goldmensch ! Durch dich haw ich nitt numme myn Gald prima plaziert, nein, dü hesch mir sogar noch myn ganz Vermeje gerett! Diss vergasse ich dir nitt ! Hytt noch lej ich üf d'r Bank fur dich 100 000 Franke vun denne Aktien do an! 
(Er umarmt’ne.)

        
"
"
          
JEAN :

          
Merci, ,patron... ! Unn wie steht’s mit d'r Kindigung ?

        
"
"
          
MEYER:

          
Na, Guten, ich b’halt dich in dem Fal nadierlich widder ..... ich habb mich zuedem so schen an dich gewehnt, unn dyner Vrstand .....

        
"
"
          
JEAN :

          
..... langt fur uns zwei!

        
"
"
          
MEYER:

          
Awer au eins merik dir fur in Zuekunft unn nemm’s ze Härze : Ich bin d'r Herr do hinne unn dü dr Ang’stellt! ’s gält also numme dyner Wille ..... äh, ich will saawe: nitt myner Wille ..... ei, nein, ich meinenen... ’s gält also numme dyner Wille ..... äh, ich na, kurz, dü weisch jo schun, wie ich’s meinenen ! ’s G'sicht vum Alfred kannsch de mynntwäje b’halte, awer syner Charakter blyet mir zuem Huus drüsse !

        
"
"
          
JEAN :

          
(setzt im Meyer d’Däschelskapp bis iwer d’Ohre üf de  Kopf)

          
Odder kurz g’saat : Mir bringe drej Kepf unde ein Kapp ! No wäre mir schun besser mitnand härmoniere ! 
(D'r Vorhang fallt g’schwind.)
 
 Schluss. 

        
"
